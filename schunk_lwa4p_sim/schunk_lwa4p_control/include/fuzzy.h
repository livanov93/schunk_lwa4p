#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include <boost/bind.hpp>
#include <time.h>


#include "control_msgs/FollowJointTrajectoryActionGoal.h"
#include "control_msgs/FollowJointTrajectoryActionFeedback.h"
#include "control_msgs/FollowJointTrajectoryActionResult.h"

#include "actionlib_msgs/GoalID.h"
#include "actionlib_msgs/GoalStatusArray.h"

#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <control_msgs/JointTrajectoryControllerState.h>



#include <ros/package.h>

#include <sstream>
#include <fstream>
#include <math.h>

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>
//#define pi 3.14159 // PI represents 3.14159
//const double PI = std::atan(1.0)*4;
//const std::complex<double> PIC(PI,0);

class Rectangle{
  public:
    Rectangle();
    void Init(double a, double c, double e);
    double ComputeR(double x);
    double A, C, E;
  protected:
    
};

Rectangle::Rectangle(){};

void Rectangle::Init(double a, double c, double e){
  A=a;  C=c;  E=e;
  std::cout<<"Rectangle Membership Function Initalized with parameters a= "<<A<<" c= "<<C<<" e= "<<E<<std::endl;
}

double Rectangle::ComputeR(double x){
  if (x<A)
    return 0;
  else if (x >= A && x < C)
    return ( (double) (x-A) )/( (double) (C - A) );
  else if (x >=C && x <= E)
    return ( (double) (E-x) )/( (double) (E-C) );
  else 
    return 0;
}

class Trapeze{
  public:
    Trapeze();
    void Init(double a, double b, double d, double e);
    double ComputeT(double x);
    double A, B, D, E;
  protected:
    
};

Trapeze::Trapeze(){};

void Trapeze::Init(double a, double b, double d, double e){
  A=a; B=b; D=d; E=e;
  std::cout<<"Trapeze Membership Function Initalized with parameters a= "<<A<<" b= "<<B<<" d= "<<D<<" e= "<<E<<std::endl;
}

double Trapeze::ComputeT(double x){
  //std::cout<<"poskusavam izracunati...";
  if (x<A)
    return 0;
  else if (x >= A && x < B)
    return ( (double) (x-A) )/( (double) (B - A) );
  else if (x >=B && x < D)
    return 1.0;
  else if (x >=D && x <= E)
    return ( (double) (E-x) )/( (double) (E-D) );
  else if (x>E)
    return 0;
}


//###############################################
//-1 * std::numeric_limits<float>::infinity();
//#################################################
class AdaptiveFuzzyController
{
 
 public:

    // Class constructor
  AdaptiveFuzzyController();
  void Init(int NumOfRules_, const std::string& path_to_singletons, const std::string& path_to_borders);
  void Load_Singletons(std::string path);
  void Load_Borders(std::string path2);
  double Return_UFC( double e, double de);
               
 protected: 

  std::vector< std::vector <double> > SINGLETONS;
  std::vector< std::vector <double> > borders;
  std::vector< Rectangle > R_E;
  std::vector< Trapeze > T_E;
  std::vector< Rectangle > R_DE;
  std::vector< Trapeze > T_DE;
  int NumOfRules;
  double Error_Max, DError_Max;

 };

 AdaptiveFuzzyController::AdaptiveFuzzyController(){}

 void AdaptiveFuzzyController::Init(int NumOfRules_, const std::string& path_to_singletons, const std::string& path_to_borders){
  std::cout<<"Creating Instance of Adaptive Fuzzy Controller...\n";

  //SINGLETONS.resize( NumOfRules_ );
  //for( std::vector<std::vector<int> >::iterator it = SINGLETONS.begin(); it != SINGLETONS.end(); ++it)
  //  it->resize( NumOfRules );
  Load_Singletons(path_to_singletons);
  Load_Borders(path_to_borders);
  //R_E.resize(NumOfRules-2);
  //T_E.resize(2);
  //R_DE.resize(NumOfRules-2);
  //T_DE.resize(2);
  NumOfRules=NumOfRules_;

  //initalizing rectangles
  for(unsigned int i=0;i<NumOfRules;i++){
    for(unsigned int j=0;j<NumOfRules;j++){
      std::cout<<SINGLETONS[i][j]<<" ";
      if (j==6)
        std::cout<<"\n";
    }


  }
  unsigned int k=0;
  //std::cout<<"R_E size is "<<NumOfRules_-2<<std::endl;
  for (unsigned int i=0; i<NumOfRules-2; i++){
    Rectangle *trokut=new Rectangle;
    trokut->Init(borders[0][0+k], borders[0][1+k],borders[0][2+k]);
    R_E.push_back(*trokut);
    trokut->Init(borders[1][0+k], borders[1][1+k],borders[1][2+k]);
    R_DE.push_back(*trokut);
    k++;
  }

  //initalizing trapezes
  Trapeze *trapez=new Trapeze;
  trapez->Init(-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(), borders[0][0], borders[0][1] );
  T_E.push_back(*trapez);
  Trapeze *trapez2=new Trapeze;
  trapez2->Init(borders[0][2+k-2], borders[0][2+k-1], std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity() );
  T_E.push_back(*trapez2);

  Trapeze *trapez3=new Trapeze;
  trapez3->Init(-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(),  borders[1][0], borders[1][1] );
  T_DE.push_back(*trapez3);
  Trapeze *trapez4=new Trapeze;
  trapez->Init(borders[1][2+k-2], borders[1][2+k-1], std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity() );
  T_DE.push_back(*trapez4);


  //TO_DO- add computinf derivation of each membership fcn so adaptation can be easier
  std::cout<<"Adaptive Fuzzy Controller Initialized...PRESS ANY KEY TO CONTINUE\n";
  std::cin.get();


}


void AdaptiveFuzzyController::Load_Singletons(std::string path){
  std::cout<<"Loading Singletons...\n";
  std::ifstream  data(path.c_str());
  std::string line;

  while(std::getline(data,line))
  {
    std::stringstream  lineStream(line);
    std::string        cell;
    std::vector<double> q(NumOfRules);
    q.clear();
    while(std::getline(lineStream,cell,','))
      q.push_back((double)std::atof(cell.c_str()));
    
    SINGLETONS.push_back(q);
  } 
}
void AdaptiveFuzzyController::Load_Borders(std::string path2){
  std::cout<<"Loading Borders...\n";

  std::ifstream  data(path2.c_str());
  std::string line;

  while(std::getline(data,line))
  {
    std::stringstream  lineStream(line);
    std::string        cell;
    std::vector<double> q(NumOfRules);
    q.clear();
    while(std::getline(lineStream,cell,','))
      q.push_back((double)std::atof(cell.c_str()));    
    borders.push_back(q);
  } 
}

double AdaptiveFuzzyController::Return_UFC( double e, double de){

  std::vector<double> v_E;//(NumOfRules);
  std::vector<double> v_DE;//(NumOfRules);  

  for (unsigned int i=0; i < NumOfRules; i++){
  
    if(i==0 ){
      double dodaj=T_E[0].ComputeT(e);
      v_E.push_back(dodaj);
      v_DE.push_back(T_DE[0].ComputeT(de));
      std::cout<<(T_E[0]).ComputeT(e)<<(T_DE[0].ComputeT(de))<<std::endl;
    }else if ( i==NumOfRules-1){
      v_E.push_back(T_E[1].ComputeT(e));
      v_DE.push_back(T_DE[1].ComputeT(de));
      std::cout<<(T_E[1]).ComputeT(e)<<(T_DE[1].ComputeT(de))<<std::endl;
    }else if(i>0 && i<(NumOfRules-1)){
      v_E.push_back(R_E[i-1].ComputeR(e));
      v_DE.push_back(R_DE[i-1].ComputeR(de));
      std::cout<<(R_E[i-1]).ComputeR(e)<<(R_DE[i-1].ComputeR(de))<<std::endl;
    }    
  }


  //Creating Propositions IF e AND de THEN u IS...

  /*for (unsigned int i=0;i<7;i++)
    std::cout<<v_E[i];

  std::cout<<"\n";


  for (unsigned int i=0;i<7;i++)
    std::cout<<v_DE[i];*/

  

  std::vector<unsigned int> I; //error iterator
  std::vector<unsigned int> J; //delta error iterator

  for (unsigned int i=0; i<NumOfRules; i++){
    if (v_E[i] !=0.0){
      I.push_back(i);
    }
    if (v_DE[i] != 0.0){
      J.push_back(i);
    }
  }

  //std::cout<<"Number of activated rules "<<I.size()+J.size()<<std::endl;

  double U_FC=0;
  double sum=0;

  for (unsigned int i=0; i<I.size(); i++){
    for (unsigned int j=0; j<J.size(); j++){
      double mi_R=std::min(v_E[I[i]], v_DE[J[i]]);
      double u = SINGLETONS[J[i]][I[i]];
      U_FC = U_FC + mi_R*u;
      sum=sum+mi_R;



    }

  }

  U_FC=U_FC/sum;

  return U_FC;






}

// std::vector<int>::iterator result = std::min_element(std::begin(v), std::end(v));
  //  std::cout << "min element at: " << std::distance(std::begin(v), result);
