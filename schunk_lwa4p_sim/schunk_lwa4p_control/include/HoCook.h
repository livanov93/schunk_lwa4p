#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Eigen>
#include <Eigen/SVD>
#include <boost/bind.hpp>
#include <math.h>

template<typename _Matrix_Type_>
_Matrix_Type_ pseudoInverse(const _Matrix_Type_ &a, double epsilon = std::numeric_limits<double>::epsilon())
{
    Eigen::JacobiSVD< _Matrix_Type_ > svd(a ,Eigen::ComputeThinU | Eigen::ComputeThinV);
    double tolerance = epsilon * std::max(a.cols(), a.rows()) *svd.singularValues().array().abs()(0);
    return svd.matrixV() *  (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
}

class Ho_Cook
{
public:
	Ho_Cook();
	bool init(Eigen::MatrixXd Q, Eigen::MatrixXd v_max, Eigen::MatrixXd a_max);
	bool start(Eigen::MatrixXd Q, Eigen::MatrixXd v_max, Eigen::MatrixXd a_max);
	double get_F(int spline, int joint, double t);
	double get_Fv(int spline, int joint, double t);
	double get_Fa(int spline, int joint, double t);
	double get_Ff(int spline, int joint, double t);
	Eigen::MatrixXd solve_Fa(int spline, int joint);
	Eigen::MatrixXd solve_Ff(int spline, int joint);
	Eigen::MatrixXd get_next_point(double freq);

	Eigen::MatrixXd T, M, H, v, a, B, M_T, Svmtx, Samtx, mtxVmax, mtxAmax, D;
	int rows, N;
	double Fvpom, Fapom, Vmax_now, Amax_now, S, Sv, Sa;
	
//private:
	
	int iter, finished_iter, spline_counter;	
	double t_hz;
	
};




Ho_Cook::Ho_Cook()
{	
    
}

bool Ho_Cook::init(Eigen::MatrixXd Q, Eigen::MatrixXd v_max, Eigen::MatrixXd a_max)
{
	rows = Q.rows();
	N = Q.cols();
    iter = 0;
    finished_iter = 0;
    t_hz = 0;
    spline_counter = 0;
	//%provjera da li ima dovoljno tocaka i velicina te matrice. Ako nema
    //%dovoljno dodaj tocke, najmanje mora biti 4 tocke.
    if (N < 4) std::cout << "Need more waypoints" << '\n'; 
    
    //%T = 0.1*ones(N-1, 1); %T2 do TN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T = Eigen::MatrixXd::Zero(N-1, 1);
    M = Eigen::MatrixXd::Zero(2*N-4, 2*N-4); //%matrica s vremenima
    M_T = Eigen::MatrixXd::Zero(2*N-4, 2*N-4);
    H = Eigen::MatrixXd::Zero(rows, 2*N-4); //%matrica s tockama
    //%D = H*M^T^-1
    
    //%matrica D sa brzinama, postavi pocetnu i krajnju, one su poznate
    v = Eigen::MatrixXd::Zero(rows, N); 
    a = Eigen::MatrixXd::Zero(rows, N); 

    //% matrica B s koeficjentima
    B = Eigen::MatrixXd::Zero(rows*(N-1), 6);

    Svmtx = Eigen::MatrixXd::Zero(rows, 1);
    Samtx = Eigen::MatrixXd::Zero(rows, 1);
    mtxVmax = Eigen::MatrixXd::Zero(rows, N-1);
    mtxAmax = Eigen::MatrixXd::Zero(rows, N-1);
    
    
   // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%puni T s parametrièkim vremenima

   
    for (int i = 0; i < N-1; i++)
    {
    	for (int j = 0; j < rows; j++)
    	{
            T(i) = T(i) + (Q(j, i+1) - Q(j, i))*(Q(j, i+1) - Q(j, i));
    	}
        T(i) = sqrt(T(i));
    }
    //T.fill(5);


    std::cout<<"Initialization of HoCook with " <<Q.size()<<" points\n"<<"Q is...\n"<<Q<<"\n T is...\n"<<T<<"\n v_max is...\n"<<v_max<<"\na_max is...\n"<<a_max<<std::endl;
}


bool Ho_Cook::start(Eigen::MatrixXd Q, Eigen::MatrixXd v_max, Eigen::MatrixXd a_max)


{

	while(finished_iter == 0 && iter < 1000)
	{    //std::cin.clear();  
        //std::cin.get();
		//std::cout << "1" << '\n'; 
		for(int i = 0; i < 1; i++)
		{
			M(i, i) = 6/T(i);
	        M(i, i+1) = -1.; 
	        M(i+1, i) = 18/pow(T(i),2) + 18/pow(T(i+1),2);
	        M(i+1, i+1) = 6/T(i+1);
	        M(i+1, i+2) = 6/pow(T(i+1),2);
		}
		//std::cout << "2" << '\n'; 
		for(int i = 2*N-5-1; i < 2*N-5; i++)
		{
			M(i, i-2) = 3/T(N-2-1);
	        M(i, i-1) = 1./2;
	        M(i, i) = 3/T(N-2-1);
	        M(i, i+1) = -1./2;
	        M(i+1, i-2) = 5/pow(T(N-2-1),2);
	        M(i+1, i-1) = 1/T(N-2-1);
	        M(i+1, i) = 3/pow(T(N-2-1),2) + 6/pow(T(N-1-1),2);
	        M(i+1, i+1) = 3/(2*T(N-1-1));
		} 
		//std::cout << "3" << '\n'; 
		//std::cout << "slijedi M " << '\n' << M << '\n';

        for (int i = 1; i < N-3; i++)
        {
        	M(2*i, 2*i-2) = 6*T(i);
            M(2*i, 2*i-1) = pow(T(i),2);
            M(2*i, 2*i) = 6*T(i);
            M(2*i, 2*i+1) = -pow(T(i),2);
            M(2*i+1, 2*i-2) = 5*T(i)*pow(T(i+1),3);
            M(2*i+1, 2*i-1) = pow(T(i),2)*pow(T(i+1),3);
            M(2*i+1, 2*i) = 3*T(i)*pow(T(i+1),3) + 3*pow(T(i),3)*T(i+1);
            M(2*i+1, 2*i+1) = pow(T(i),3)*pow(T(i+1),2);
            M(2*i+1, 2*i+2) = pow(T(i),3)*T(i+1);
        }
       // std::cout << "4" << '\n'; 
        //std::cout << "slijedi M " << '\n' << M << '\n';

        M_T = M.transpose();
       // std::cout << "slijedi M " << '\n' << M << '\n';
        //std::cout << "5" << '\n'; 
        ///////////////////////////////////////////fill the H
        for(int i = 0; i < 1; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		H(j, i) = 12/pow(T(i),2)*(Q(j, i+1) - Q(j, i)) - 6/T(i)*v(j, i) - a(j, i);
		        H(j, i+1) = 48/pow(T(i),3)*(Q(j, i+1) - Q(j, i)) + 24/pow(T(i+1),3)*(Q(j, i+2) - Q(j, i+1)) - 30/pow(T(i),2)*v(j, i) - 6/T(i)*a(j, i); 
        	}
        }
        //std::cout << "6" << '\n'; 
        for(int i = 2*N-5-1; i < 2*N-5; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		H(j, i) = 6/pow(T(N-2-1),2)*(Q(j, N-1-1) - Q(j, N-2-1));
		        H(j, i+1) = 8/pow(T(N-2-1),3)*(Q(j, N-1-1) - Q(j, N-2-1)) + 10/pow(T(N-1-1),3)*(Q(j, N-1) - Q(j, N-1-1)) - 4/pow(T(N-1-1),2)*v(j, N-1) + 1./(2*T(N-1-1))*a(j, N-1);
        	}
        }
        //std::cout << "7" << '\n'; 
        for(int i = 1; i < N-3; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		H(j, 2*i) = 12*(Q(j, i+1) - Q(j, i));
            	H(j, 2*i+1) = 8*pow(T(i+1),3)*(Q(j, i+1) - Q(j, i)) + 4*pow(T(i),3)*(Q(j, i+2) - Q(j, i+1));
        	}        	
        }
        //std::cout << "8" << '\n'; 
        //std::cout << "slijedi H " << '\n' << H << '\n';

        //%izracun D i postavljanje vektora brzina i acc
        D = H*pseudoInverse(M_T);
        //std::cout << "9" << '\n'; 
        //std::cout << "slijedi D " << '\n' << D << '\n';

        for(int i = 1; i < N-1; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		v(j, i) = D(j, 2*i-2);
        		a(j, i) = D(j, 2*i-1);
        	}
        }
        //std::cout << "8" << '\n'; 
        //std::cout << "slijedi v " << '\n' << v << '\n';

        //std::cout << "slijedi a " << '\n' << a << '\n';
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%puni B 
        for(int i = 0; i < (N-1)-1; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		B(rows*i+j, 0) = Q(j,i);
        		B(rows*i+j, 1) = v(j,i);
        		B(rows*i+j, 2) = 1./2*a(j,i);
        		B(rows*i+j, 3) = 4*(Q(j, i+1) - Q(j, i))/pow(T(i),3) - 3*v(j, i)/pow(T(i),2) - a(j, i)/T(i) - v(j, i+1)/pow(T(i),2);
        		B(rows*i+j, 4) = 3*(Q(j, i) - Q(j, i+1))/pow(T(i),4) + 2*v(j, i)/pow(T(i),3) + a(j, i)/(2*pow(T(i),2)) + v(j, i+1)/pow(T(i),3);
        	}
        }
        //std::cout << "7" << '\n'; 
        for(int i = N-1-1; i < N-1; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		B(rows*i+j, 0) = Q(j, i);
		        B(rows*i+j, 1) = v(j, i);
		        B(rows*i+j, 2) = 1./2*a(j, i);
		        B(rows*i+j, 3) = 10*(Q(j, i+1) - Q(j, i))/pow(T(i),3) - 6*v(j, i)/pow(T(i),2) - 3*a(j, i)/(2*T(i)) - 4*v(j, i+1)/pow(T(i),2) + a(j, i+1)/(2*T(i));
		        B(rows*i+j, 4) = 15*(Q(j, i) - Q(j, i+1))/pow(T(i),4) + 8*v(j, i)/pow(T(i),3) + 3*a(j, i)/(2*pow(T(i),2)) + 7*v(j, i+1)/pow(T(i),3) - a(j, i+1)/pow(T(i),2);
		        B(rows*i+j, 5) = 6*(Q(j, i+1) - Q(j, i))/pow(T(i),5) - 3*v(j, i)/pow(T(i),4) - a(j, i)/(2*pow(T(i),3)) - 3*v(j, i+1)/pow(T(i),4) + a(j, i+1)/(2*pow(T(i),3));
        	}
        }
        //std::cout << "6" << '\n'; 
        //std::cout << "slijedi B " << '\n' << B<< '\n';
  		
  		//finished_iter = 1;
        
        for(int i = 0; i < N-1; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		//std::cout << "i " << i << '\n';
        		//std::cout << "j " << j << '\n';

               // std::cout << "slijedi T " << '\n' << T << '\n';

                //std::cin.clear();
                //std::cin.get();

                //std::cout<<"i is "<<i<<" j is "<<j<<"\n";
        		Fvpom = std::abs(get_Fv(i, j, 0));

        		Fapom = std::abs(get_Fa(i, j, 0));

                //std::cout<<"Fvpom is "<<Fvpom<<"\n";

                //std::cout<<"Fapom is "<<Fapom<<"\n";
                
        		//std::cout << "23" << '\n';

        		Vmax_now = Fvpom;
        		Amax_now = Fapom;


                

        		Fvpom = std::abs(get_Fv(i, j, T(i)));
        		//std::cout << "samo" << get_Fv(i, j, T(i))<<'\n';
        		Fapom = std::abs(get_Fa(i, j, T(i)));
        		//std::cout << "2" << '\n';


                //std::cout<<"Fvpom2 is "<<Fvpom<<"\n";

                //std::cout<<"Fapom2 is "<<Fapom<<"\n";

        		if(Fvpom > Vmax_now) Vmax_now = Fvpom;
        		if(Fapom > Amax_now) Amax_now = Fapom;

                //std::cout <<"Vmax_now is..."<< Vmax_now<< "\n";
                //std::cout <<"Amax_now is..."<< Amax_now<< '\n';

        		//std::cout << "22" << '\n';
        		Eigen::MatrixXd Vresult = solve_Fa(i, j);


                //std::cout<<"Vresult is "<<Vresult<<"\n";
        		//std::cout << "23" << '\n';
        		for(int k = 0; k < Vresult.rows(); k++)
        		{
        			if(Vresult(k) > 0 && Vresult(k) < T(i))
        			{
        				Fvpom = std::abs(get_Fv(i, j, Vresult(k)));
        				if (Fvpom > Vmax_now) Vmax_now = Fvpom;
        			}
        		}
        		//std::cout << "3" << '\n';
        		Eigen::MatrixXd Aresult = solve_Ff(i, j);

                //std::cout<<"Aresult is "<<Aresult<<"\n";

        		for(int k = 0; k < Aresult.rows(); k++)
        		{
        			if(Aresult(k) > 0 && Aresult(k) < T(i))
        			{
        				Fapom = std::abs(get_Fa(i, j, Aresult(k)));
        				if (Fapom > Amax_now) Amax_now = Fapom;
        			}
        		}
        //		std::cout << "4" << '\n';
               
        		mtxVmax(j,i) = Vmax_now/v_max(j);
        		mtxAmax(j,i) = Amax_now/a_max(j); //tu sam drugacije, provjeri!!
        	}
        }
        //std::cout << "slijedi mtxVmax " << '\n' << mtxVmax << '\n';
        //std::cout << "slijedi mtxAmax " << '\n' << mtxAmax << '\n';
        Sv = 0;
        Sa = 0;
        for(int i = 0; i < N-1; i++)
        {
        	for(int j = 0; j < rows; j++)
        	{
        		if (mtxVmax(j,i) > Sv) Sv = mtxVmax(j,i);
        		if (mtxAmax(j,i) > Sa) Sa = mtxAmax(j,i);
        	}
        }
        Sa = sqrt(Sa);

        S = Sv;
        if (Sa > Sv) S = Sa;

        std::cout<<"\nSa je "<<Sa<<"\nSv je"<<Sv<<std::endl;

        std::cout << "slijedi S: " << S << '\n';
        if (S > 0.98 && S < 1.02) finished_iter = 1;            
        else
        {	
        	//if (S>2) S = 2;
        	iter = iter + 1;
            std::cout<<"\n Iteration"<<iter<<"\n";
            T = T * S;
        }   
        //std::cout << "slijedi T " << '\n' << T << '\n';
        //usleep(30000000);
	}
	

	return 1;
}

double Ho_Cook::get_F(int spline, int joint, double t)
{
	//rows 0 <-> N-2
	double t2 = t*t;
	double t3 = t*t*t;
	double t4 = t*t*t*t;
	double t5 = t*t*t*t*t;

    return (B(rows*spline+joint, 0) + B(rows*spline+joint, 1)*t + B(rows*spline+joint, 2)*t2 + B(rows*spline+joint, 3)*t3 + B(rows*spline+joint, 4)*t4 + B(rows*spline+joint, 5)*t5);
}

double Ho_Cook::get_Fv(int spline, int joint, double t)
{
	//rows 0 <-> N-2
	double t2 = t*t;
	double t3 = t*t*t;
	double t4 = t*t*t*t;
/*
    std::cout<<"rows*spline+joint\n"<<rows*spline+joint<<"\n";

    std::cout<<"B(rows*spline+joint, 1)\n"<<B(rows*spline+joint, 1)<<"\n";

    std::cout<<"2*B(rows*spline+joint, 2)\n"<<2*B(rows*spline+joint, 2)*1<<"\n";

    std::cout<<"3*B(rows*spline+joint, 3)\n"<<3*B(rows*spline+joint, 3)*1<<"\n";

    std::cout<<"4*B(rows*spline+joint, 4)\n"<<4*B(rows*spline+joint, 4)*1<<"\n";

    std::cout<<"5*B(rows*spline+joint, 5)\n"<<5*B(rows*spline+joint, 5)*1<<"\n";

    std::cout<<"t\n"<<t<<"\n";

    std::cout<<"ret\n"<<(B(rows*spline+joint, 1) + 2*B(rows*spline+joint, 2)*t + 3*B(rows*spline+joint, 3)*t2 + 4*B(rows*spline+joint, 4)*t3 + 5*B(rows*spline+joint, 5)*t4)<<"\n";

    //std::cout << "slijedi B " << '\n' << B<< '\n';*/
    

   

    return (B(rows*spline+joint, 1) + 2*B(rows*spline+joint, 2)*t + 3*B(rows*spline+joint, 3)*t2 + 4*B(rows*spline+joint, 4)*t3 + 5*B(rows*spline+joint, 5)*t4);
}

double Ho_Cook::get_Fa(int spline, int joint, double t)
{
	//rows 0 <-> N-2
	double t2 = t*t;
	double t3 = t*t*t;

	return (2*B(rows*spline+joint, 2) + 3*2*B(rows*spline+joint, 3)*t + 4*3*B(rows*spline+joint, 4)*t2 + 5*4*B(rows*spline+joint, 5)*t3);
}

double Ho_Cook::get_Ff(int spline, int joint, double t)
{
	//rows 0 <-> N-2
	double t2 = t*t;

    return (3*2*B(rows*spline+joint, 3) + 4*3*2*B(rows*spline+joint, 4)*t + 5*4*3*B(rows*spline+joint, 5)*t2);
}
Eigen::MatrixXd Ho_Cook::solve_Fa(int spline, int joint)
{
	double a = 5*4*B(rows*spline+joint, 5);
	double b = 4*3*B(rows*spline+joint, 4);
	double c = 3*2*B(rows*spline+joint, 3);
	double d = 2*B(rows*spline+joint, 2);
	if (a < 1e-3 && a > -1e-3) return Eigen::MatrixXd::Zero(1,1);

	
	double p = -b/(3*a);
	double q = pow(p,3) + (b*c - 3*a*d)/(6*pow(a,2));
	double r = c/(3*a);

	double det = pow(q,2) + pow((r - pow(p,2)),3);
	if (det < 0) return Eigen::MatrixXd::Zero(1,1);

	Eigen::MatrixXd result(1,1);
		
	result(0) = cbrt(q + sqrt(det)) + cbrt(q - sqrt(det)) + p;
	return result;	
}

Eigen::MatrixXd Ho_Cook::solve_Ff(int spline, int joint)
{
	double a = 5*4*3*B(rows*spline+joint, 5);
	double b = 4*3*2*B(rows*spline+joint, 4);
	double c = 3*2*B(rows*spline+joint, 3);
	if (a < 1e-3 && a > -1e-3) return Eigen::MatrixXd::Zero(1,1);

	double det = pow(b,2) - 4*a*c;
	if (det < 0) return Eigen::MatrixXd::Zero(1,1);

	Eigen::MatrixXd result(2,1);

	result(0) = (-b - sqrt(det))/(2*a);
	result(1) = (-b + sqrt(det))/(2*a);
	return result;
}

Eigen::MatrixXd Ho_Cook::get_next_point(double freq)
{
	Eigen::MatrixXd point_ret;
	point_ret = Eigen::MatrixXd::Zero(6,4); //last row indicates if last spline is out

	for(int j = 0; j < rows; j++)
	{
		point_ret(j,0) = get_F(spline_counter, j, t_hz);
		point_ret(j,1) = get_Fv(spline_counter, j, t_hz);
		point_ret(j,2) = get_Fa(spline_counter, j, t_hz);
	}

	t_hz += 1./freq;

	if(t_hz > T(spline_counter))
	{
		t_hz -= T(spline_counter);
		spline_counter++;

		if(spline_counter == N-1)
		{
			std::cout << "last waypoint" << '\n';
			point_ret.col(1).fill(0); //vel and acc = 0
			point_ret.col(2).fill(0);
			point_ret.col(3).fill(1); //indicates that is last point
		}
		else if (T(spline_counter) < 1./freq) std::cout << "spline time is less than freq. " << T(spline_counter) << '\n';
	}

	return point_ret;
}


