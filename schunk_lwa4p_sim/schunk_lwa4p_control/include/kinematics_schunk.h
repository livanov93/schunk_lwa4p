#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include <boost/bind.hpp>
#include <time.h>


#include "control_msgs/FollowJointTrajectoryActionGoal.h"
#include "control_msgs/FollowJointTrajectoryActionFeedback.h"
#include "control_msgs/FollowJointTrajectoryActionResult.h"

#include "actionlib_msgs/GoalID.h"
#include "actionlib_msgs/GoalStatusArray.h"

#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <control_msgs/JointTrajectoryControllerState.h>



#include <ros/package.h>

#include <sstream>
#include <math.h>

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>

#include "fuzzy.h"
#include "HoCook.h"
//#define pi 3.14159 // PI represents 3.14159
const double PI = std::atan(1.0)*4;
const std::complex<double> PIC(PI,0);

class ImpedanceController
{
 
 public:

    // Class constructor
  ImpedanceController(ros::NodeHandle& n, ros::NodeHandle& n_ns);

    
  //void JointStateCallback(const control_msgs::JointTrajectoryControllerStatePtr& message_state);
  void JointStateCallback(const sensor_msgs::JointState::ConstPtr& msg);


  void ForceCallback(const geometry_msgs::WrenchStamped::ConstPtr& msg);    
  
  Eigen::MatrixXd direct_k(Eigen::MatrixXd q);

  Eigen::VectorXd ik_closest(const Eigen::VectorXd& w1, const Eigen::VectorXd& q0);

  Eigen::MatrixXd inverse_k(const Eigen::VectorXd& w); 

  void impedance_filter(const Eigen::VectorXd& E, const Eigen::VectorXd& X_R);

  void pv_filter(Eigen::VectorXd& F_M);

  Eigen::VectorXd Jacobian(const Eigen::VectorXd& q, const Eigen::VectorXd& dw);

  void feedback_callback(const control_msgs::FollowJointTrajectoryActionFeedbackPtr& message_status);
  //*void state_callback(const control_msgs::JointTrajectoryControllerStatePtr& message_state);

  void status_callback(const actionlib_msgs::GoalStatusArrayPtr& message_status);

  Eigen::MatrixXd taylor_path(Eigen::MatrixXd q1_tay, Eigen::MatrixXd w2_tay, double tol_tay, int nrec_tay);

  Eigen::MatrixXd taylor_path_multPoint(Eigen::MatrixXd path_tay, double tol_tay);

  Eigen::MatrixXd ik_path(Eigen::MatrixXd w_ik_path);
 
               
 //private: 
  //####################################################################################################################
  ros::Publisher pub_traj_;
  ros::Publisher pub_actual_vel_;
  ros::Subscriber get_state_;
  ros::Subscriber get_status_;
  ros::Subscriber get_feedback_;
  std::string action_name_follow_;

  control_msgs::FollowJointTrajectoryActionGoal traj_vector;

  int DOF; 
  uint8_t status, status_f, status_old;

  std::vector<double> q_current;  
  std::vector<double> q_last;

  //###################################################################################################################3 

  ros::Time Start, End;
  ros::Publisher position_pub, pose_pub, joint_1_cmd, joint_2_cmd, joint_3_cmd, joint_4_cmd, joint_5_cmd, joint_6_cmd;
  ros::Subscriber joint_sub, force_sub;
  //std_msgs::Float64MultiArray ToolPose;
  geometry_msgs::PoseStamped TP;
  double eps;

  std_msgs::Float64 q1_cmd, q2_cmd, q3_cmd, q4_cmd, q5_cmd, q6_cmd;

  Eigen::VectorXd Q_current;

  //variables for impedance filter 
  //MDS system --> M, B, K
  Eigen::MatrixXd M, B, K;
  Eigen::MatrixXd a, b, c, d;
  Eigen::VectorXd X_C_k_minus1, X_C_k_minus2, X_C_k;
  Eigen::VectorXd E_k_minus1, E_k_minus2, E_k;
  Eigen::VectorXd Ui_k_minus1, Ui, Up, Ud;
  Eigen::VectorXd XR;
  Eigen::VectorXd FR;
  bool FirstPass, SecondPass;
  double T_d, kp,kd, ki;
  ros::Time T_old;

  ros::Time T_old_f, T_traj;
  Eigen::VectorXd F_pv_k, F_pv_k_minus1;
  double T_f;
  bool znopra;
  std_msgs::Header last_traj;
  bool makse;
  bool check_status;
  bool status_rdy;

  std::vector<std::vector <double> > POSITION;

  AdaptiveFuzzyController AFC;

  Eigen::MatrixXd path_tay_ret;
  bool unfeasible;
  Eigen::MatrixXd  p, vmax, amax;
  double tol;

  Ho_Cook *hc; 
  bool last_pub;
  bool ref_passed;

  Eigen::VectorXd WD;

  ros::Publisher reference_model_publisher, reference_publisher;
  double T_model_konst;
  ros::Time T_model_old;
  Eigen::MatrixXd F_model, F_model_old;

};
