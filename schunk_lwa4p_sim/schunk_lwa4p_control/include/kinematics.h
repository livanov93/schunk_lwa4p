#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/WrenchStamped.h"
#include <boost/bind.hpp>


#include <ros/package.h>

#include <sstream>
#include <math.h>

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>
//#define pi 3.14159 // PI represents 3.14159
const double PI = std::atan(1.0)*4;
const std::complex<double> PIC(PI,0);

class ImpedanceController
{
 
 public:

    // Class constructor
  ImpedanceController(ros::NodeHandle& n, ros::NodeHandle& n_ns);

    
  void JointStateCallback(const sensor_msgs::JointState::ConstPtr& msg);


  void ForceCallback(const geometry_msgs::WrenchStamped::ConstPtr& msg);    
  
  void direct_k(double q[], double w[]);

  Eigen::VectorXd ik_closest(const Eigen::VectorXd& w1, const Eigen::VectorXd& q0);

  Eigen::MatrixXd inverse_k(const Eigen::VectorXd& w); 

  void impedance_filter(const Eigen::VectorXd& E, const Eigen::VectorXd& X_R);

  void pv_filter(Eigen::VectorXd& F_M);

  Eigen::VectorXd Jacobian(const Eigen::VectorXd& q, const Eigen::VectorXd& dw);
               
 protected: 

  ros::Time Start, End;
  ros::Publisher position_pub, pose_pub, joint_1_cmd, joint_2_cmd, joint_3_cmd, joint_4_cmd, joint_5_cmd, joint_6_cmd;
  ros::Subscriber joint_sub, force_sub;
  //std_msgs::Float64MultiArray ToolPose;
  geometry_msgs::PoseStamped TP;
  double eps;

  std_msgs::Float64 q1_cmd, q2_cmd, q3_cmd, q4_cmd, q5_cmd, q6_cmd;

  Eigen::VectorXd Q_current;

  //variables for impedance filter 
  //MDS system --> M, B, K
  Eigen::MatrixXd M, B, K;
  Eigen::MatrixXd a, b, c, d;
  Eigen::VectorXd X_C_k_minus1, X_C_k_minus2, X_C_k;
  Eigen::VectorXd E_k_minus1, E_k_minus2, E_k;
  Eigen::VectorXd XR;
  Eigen::VectorXd FR;
  bool FirstPass, SecondPass;
  double T_d, kp,kd;
  ros::Time T_old;

  ros::Time T_old_f;
  Eigen::VectorXd F_pv_k, F_pv_k_minus1;
  double T_f;
};
