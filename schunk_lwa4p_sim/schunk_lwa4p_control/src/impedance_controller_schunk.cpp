#include "kinematics_schunk.h"
//#include "fuzzy.h"


#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Duration.h"

#include "control_msgs/FollowJointTrajectoryActionGoal.h"
#include "control_msgs/FollowJointTrajectoryActionFeedback.h"
#include "control_msgs/FollowJointTrajectoryActionResult.h"

#include "actionlib_msgs/GoalID.h"
#include "actionlib_msgs/GoalStatusArray.h"

#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <control_msgs/JointTrajectoryControllerState.h>



#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Eigen>
#include <Eigen/SVD>
#include <boost/bind.hpp>







ros::NodeHandle *nh_p;

double wrapToPI( double angle )
{
   int num2pi = floor(angle/(2*PI) + 0.5);
   return angle - num2pi*2*PI;
}

std::complex<double> sign(std::complex<double> a){
  if (a.real() == 0 && a.imag() == 0 ) 
    return (std::complex<double>) 0;
  else 
    return a/(std::abs(a));
}

double Atan2(double a, double b){
  return atan2(a,b);
}

std::complex<double> Asin (std::complex<double> a)
{
  std::complex<double> i(0,1);
  return -i*log(i*a +pow((std::complex<double>)1-pow(a,2),0.5));
}

std::complex<double> Acos (std::complex<double> a)
{
  std::complex<double> i(0,1);
  return -i*log(a +i*pow((std::complex<double>)1-pow(a,2),0.5));
}

/*template<typename _Matrix_Type_>
_Matrix_Type_ pseudoInverse(const _Matrix_Type_ &a, double epsilon = std::numeric_limits<double>::epsilon())
{
    Eigen::JacobiSVD< _Matrix_Type_ > svd(a ,Eigen::ComputeThinU | Eigen::ComputeThinV);
    double tolerance = epsilon * std::max(a.cols(), a.rows()) *svd.singularValues().array().abs()(0);
    return svd.matrixV() *  (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
}*/




ImpedanceController::ImpedanceController (ros::NodeHandle& n, ros::NodeHandle& n_ns)
{


  //SUBSCRIBERS
  get_state_ = n.subscribe("/joint_states", 100, &ImpedanceController::JointStateCallback, this);// n.subscribe("/pos_based_pos_traj_controller_arm/state", 100, &ImpedanceController::JointStateCallback, this);
  force_sub = n.subscribe("/optoforce_node/OptoForceWrench", 100, &ImpedanceController::ForceCallback, this);
  get_feedback_ = n.subscribe("/pos_based_pos_traj_controller_arm/follow_joint_trajectory/feedback", 100, &ImpedanceController::feedback_callback, this);  //pos_based_pos_traj_controller_arm

  get_status_ = n.subscribe("/pos_based_pos_traj_controller_arm/follow_joint_trajectory/status", 5, &ImpedanceController::status_callback, this); ///pos_based_pos_traj_controller_arm

  //PUBLISHERS 
  pose_pub = n.advertise<geometry_msgs::PoseStamped>("TP", 100);
  pub_traj_ = n.advertise<control_msgs::FollowJointTrajectoryActionGoal>("/pos_based_pos_traj_controller_arm/follow_joint_trajectory/goal", 100); ///pos_based_pos_traj_controller_arm

  reference_model_publisher = n.advertise<geometry_msgs::WrenchStamped>("/reference_model", 100);
  reference_publisher=  n.advertise<geometry_msgs::WrenchStamped>("/reference_force", 100);


  AFC.Init(7, "/home/lovro/catkin_ws/src/schunk_lwa4p/schunk_lwa4p_sim/schunk_lwa4p_control/src/singletons.txt", "/home/lovro/catkin_ws/src/schunk_lwa4p/schunk_lwa4p_sim/schunk_lwa4p_control/src/borders.txt");



  //#############################################################################################

  action_name_follow_="follow_joint_trajectory";
  DOF=6;
  status = 0;
  q_current.resize(DOF, 0.0);
  q_last.resize(DOF, 0.0);

 //###############################################################################################
  

  eps=1e-16;

  //initalizing impedance filter

  X_C_k_minus1 = Eigen::VectorXd::Zero(3);
  X_C_k_minus2 = Eigen::VectorXd::Zero(3);
  X_C_k =Eigen::VectorXd::Zero(3);
  

  E_k = Eigen::VectorXd::Zero(3);
  E_k_minus1 = E_k; 
  XR=Eigen::VectorXd::Zero(3);
  FR=Eigen::VectorXd::Zero(3);

  WD=Eigen::VectorXd::Zero(6);

  
  Ui_k_minus1=Eigen::VectorXd::Zero(3);
  Ui=Eigen::VectorXd::Zero(3);
  Up=Eigen::VectorXd::Zero(3);
  Ud=Eigen::VectorXd::Zero(3);


  T_model_old = ros::Time::now();
  F_model = Eigen::MatrixXd::Zero(1,1);
  F_model_old = Eigen::MatrixXd::Zero(1,1);
  T_model_konst=0.5;
 
  
  
  M = Eigen::MatrixXd::Identity(3,3);//.cast<double>();
  B = Eigen::MatrixXd::Identity(3,3);//.cast<double>();
  K = Eigen::MatrixXd::Identity(3,3);//.cast<double>();

  double m_param; n.getParam("/impedance_controller_schunk/M",m_param);
  double b_param; n.getParam("/impedance_controller_schunk/B",b_param);
  double k_param;   n.getParam("/impedance_controller_schunk/K",k_param);
  double kp_param; n.getParam("/impedance_controller_schunk/kp",kp_param);
  double kd_param; n.getParam("/impedance_controller_schunk/kd",kd_param);
  double ki_param; nh_p->getParam("/impedance_controller_schunk/ki",ki_param);
  double fr_param; n.getParam("/impedance_controller_schunk/FR",fr_param);
  double xr_param; n.getParam("/impedance_controller_schunk/XR",xr_param);
  double yr_param; nh_p->getParam("/impedance_controller_schunk/YR",yr_param);  
  double zr_param; nh_p->getParam("/impedance_controller_schunk/ZR",zr_param);

  double e1; nh_p->getParam("/impedance_controller_schunk/e1",e1);
  double e2; nh_p->getParam("/impedance_controller_schunk/e2",e2);
  double e3; nh_p->getParam("/impedance_controller_schunk/e3",e3);

  double offsetx; nh_p->getParam("/impedance_controller_schunk/offsetx",offsetx);

  double offsety; nh_p->getParam("/impedance_controller_schunk/offsety",offsety);

  double fr_paramx; nh_p->getParam("/impedance_controller_schunk/FRx",fr_paramx);
  double fr_paramy; nh_p->getParam("/impedance_controller_schunk/FRy",fr_paramy);

  
  XR<<xr_param,yr_param,zr_param;
  FR<<fr_param, fr_paramy, fr_paramx;

  WD<< XR,e1,e2,e3;
  X_C_k=XR;
  X_C_k_minus2=X_C_k;
  X_C_k_minus1=X_C_k;

  //std::cout<<"DO ZUUU";
 
 
  M(0,0)=1*m_param;
  B(0,0)=1*b_param;  
  K(0,0)=1*k_param;

  M(1,1)=1*m_param;
  B(1,1)=1*b_param;  
  K(1,1)=1*k_param;

  M(2,2)=1*m_param;
  B(2,2)=1*b_param;  
  K(2,2)=1*k_param;

  kp=kp_param ; kd=kd_param; ki=ki_param;

  FirstPass = true; 
  SecondPass = false;
  T_d= 0.02; //discretization
  T_old=ros::Time::now();

  Q_current=Eigen::VectorXd::Zero(6);


  F_pv_k =Eigen::VectorXd::Zero(1);
  F_pv_k_minus1 = F_pv_k;
  T_f = 0.15;
  T_old_f = ros::Time::now();

  T_traj = ros::Time::now();
  unfeasible=false;
  
  //znopra=0;

  //last_traj.stamp.sec=0; last_traj.stamp.nsec=0;

  //makse=false;

    std::ifstream  data("/home/lovro/catkin_ws/src/schunk_lwa4p/schunk_lwa4p_sim/schunk_lwa4p_control/src/taylor_data.txt");

    std::string line;
    while(std::getline(data,line))
    {
        std::stringstream  lineStream(line);
        std::string        cell;
        std::vector<double> q(6);
        q.clear();
        while(std::getline(lineStream,cell,','))
        {
            // You have a cell!!!!
          q.push_back((double)std::atof(cell.c_str()));


        }
        POSITION.push_back(q);
    }
    std::vector<double> v(6);
    for (unsigned int i=0; i<POSITION.size();i++ ){
      //std::cout<<"ROW "<<i+1<<std::endl;
      for (unsigned int j=0; j<6;j++ ){
        v[j]=POSITION[i][j];
        //std::cout<<v[j]<<std::endl;
      }
    }

    check_status=true;
    status_rdy=true;
    last_pub=false;

   /* Eigen::MatrixXd result(6,1);
    Eigen::MatrixXd q000(6,1), w2(6,1);
    w2.col(0)<<-800,0,440,-1,0,0;
    q000.col(0)<<0.557011600001061,0.967466059882550,1.188271878582337,0.845450870457404,0.784538393661872, 0;


    result=taylor_path(q000,w2,0.001,0);


    std::cout<<"\nVelicina izlaza taylora je "<<result.cols()<<std::endl;
    std::cout<<result;

    std::cin.get();*/

    vmax = Eigen::MatrixXd::Zero(6,1);
    amax = Eigen::MatrixXd::Zero(6,1);
    p = Eigen::MatrixXd::Zero(6,2);
    p.col(0) << -835,-300,240,-1,0,0;
    p.col(1) << -835,0,440,-1,0,0; 
//    p.col(2) << -800,300,240,-1,0,0; 

    
    //init ho cook
    Eigen::MatrixXd q_traj, q_tay;
    q_traj = ik_path(p);
    vmax.fill(0.1);
    amax.fill(0.1);
    tol = 0.01;

    
   q_tay = taylor_path_multPoint(q_traj, tol);

   //q_tay.row(2)=-q_tay.row(2);
   

    std::cout<<"Taylor za vise tocaka vrati...\n"<<q_tay<<std::endl;
    std::cout<<"Taylor za vise tocaka velicine...\n"<<q_tay.cols()<<std::endl;

    //std::cin.clear();
    //std::cin.get();

    ref_passed=false;
  


    
  //hc= new Ho_Cook();
  //hc->init(q_tay, vmax, amax);
  //sleep(5.0);
  //hc->start(q_tay, vmax, amax);
  //sleep(5.0);
  //Eigen::MatrixXd q_hc = hc->get_next_point(50);
  /*std::cout<<"\n"<<q_hc<<std::endl; 
  //q_hc = hc->get_next_point(50);
  std::cout<<"\n"<<q_hc<<std::endl; 
  //q_hc = hc->get_next_point(50);
  std::cout<<"\n"<<q_hc<<std::endl; 
*/
  //std::cout<<"\nTimes for every spline are ...\n"<<hc->T<<std::endl;
   //std::cout<<"\nTimes size ...\n"<<hc->T.size()<<std::endl;

  //std::cout<<"\nB for every spline are ...\n"<<hc->B<<std::endl;

  /*traj_vector = {};
  traj_vector.goal.trajectory.joint_names.push_back("arm_1_joint");
  traj_vector.goal.trajectory.joint_names.push_back("arm_2_joint");
  traj_vector.goal.trajectory.joint_names.push_back("arm_3_joint");
  traj_vector.goal.trajectory.joint_names.push_back("arm_4_joint");
  traj_vector.goal.trajectory.joint_names.push_back("arm_5_joint");
  traj_vector.goal.trajectory.joint_names.push_back("arm_6_joint");*/

  std::cout<<"\nImpedance controller initialized...PRESS ANY KEY TO CONTINUE\n";
  std::cin.clear();
  std::cin.get();
  
}



void ImpedanceController::status_callback(const actionlib_msgs::GoalStatusArrayPtr& message_status)
  {
    if (!message_status->status_list.empty())

    {
      //std::cout<<"DULJINA STATUSA JE ... "<< message_status->status_list.size()<<std::endl;
      status_old= status;
      status = message_status->status_list[message_status->status_list.size() -1].status;
    }else{

      znopra=1;
      
    }
  }

void ImpedanceController::feedback_callback(const control_msgs::FollowJointTrajectoryActionFeedbackPtr& message_status)
  {
    status_f = message_status->status.status;
  }

//void ImpedanceController::JointStateCallback(const control_msgs::JointTrajectoryControllerStatePtr& message_state)

void ImpedanceController::JointStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
  double q[6];
  //double w[6];
  //double w_desired[]={   -505,0,1000,-1,0, 0.0000};	

  //Start=ros::Time::now();

  //copying position
  /*std::vector<double> positions = message_state->actual.positions;
  for (unsigned int i = 0; i < positions.size(); i++)
  { 
    if(i==2){
      q_current[i] = -positions[i];
      q[i] = -positions[i];
    } else{
      q_current[i] = positions[i];
      q[i] = positions[i];

    }*/
  for (unsigned int i = 0; i < 6; i++)
  { 
    if(i==2){
      q_current[i] = -(double)(msg->position[i]);
      q[i] = -(double)(msg->position[i]);
    } else{
      q_current[i] = (double)(msg->position[i]);
      q[i] = (double)(msg->position[i]);

    }

  
  }
  

  
 
  //Eigen::VectorXd WD= Eigen::VectorXd::Map(&w_desired[0], 6);
  Q_current = Eigen::VectorXd::Map(&q[0], 6);
  


  Eigen::MatrixXd w =Eigen::MatrixXd::Zero(6,1);
  w = direct_k(Q_current);
  //std::cout<<"X = "<<w[0]<<" \nY = "<<w[1]<<" \nZ = "<<w[2]<<" \nEuler 1 = "<<w[3]<<" \nEuler 2 = "<<w[4]<<" \nEuler 3 = "<<w[5]<<std::endl;

  //TP is tool pose
  TP.pose.position.x=(double)w(0,0);
  TP.pose.position.y=(double)w(1,0);
  TP.pose.position.z=(double)w(2,0);

  TP.pose.orientation.x=(double)w(3,0);
  TP.pose.orientation.y=(double)w(4,0);
  TP.pose.orientation.z=(double)w(5,0);
  TP.pose.orientation.w=1000.01;

  TP.header.stamp = ros::Time::now();
  TP.header.frame_id = "/world";

  pose_pub.publish(TP);

  //End=ros::Time::now();


}




void ImpedanceController::ForceCallback(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{

	Eigen::VectorXd F_M(3);// F_Mx(1), F_My(1);	
	Eigen::VectorXd E(3);	
	
  //Eigen::MatrixXd w_old;

  double m_param;  nh_p->getParam("/impedance_controller_schunk/M",m_param);
  double b_param;  nh_p->getParam("/impedance_controller_schunk/B",b_param);
  double k_param;  nh_p->getParam("/impedance_controller_schunk/K",k_param);
  double kp_param; nh_p->getParam("/impedance_controller_schunk/kp",kp_param);
  double kd_param; nh_p->getParam("/impedance_controller_schunk/kd",kd_param);
  double ki_param; nh_p->getParam("/impedance_controller_schunk/ki",ki_param);
  double fr_param; nh_p->getParam("/impedance_controller_schunk/FR",fr_param);
  double xr_param; nh_p->getParam("/impedance_controller_schunk/XR",xr_param);	
  double yr_param; nh_p->getParam("/impedance_controller_schunk/YR",yr_param);  
  double zr_param; nh_p->getParam("/impedance_controller_schunk/ZR",zr_param); 
  double fc_param; nh_p->getParam("/impedance_controller_schunk/fc",fc_param);  
  double use_jacobian; nh_p->getParam("/impedance_controller_schunk/J",use_jacobian);
  double move; nh_p->getParam("/impedance_controller_schunk/move",move);

  double e1; nh_p->getParam("/impedance_controller_schunk/e1",e1);
  double e2; nh_p->getParam("/impedance_controller_schunk/e2",e2);
  double e3; nh_p->getParam("/impedance_controller_schunk/e3",e3);
  double offset; nh_p->getParam("/impedance_controller_schunk/offset",offset);

  double offsetx; nh_p->getParam("/impedance_controller_schunk/offsetx",offsetx);

  double offsety; nh_p->getParam("/impedance_controller_schunk/offsety",offsety);

  double fr_paramx; nh_p->getParam("/impedance_controller_schunk/FRx",fr_paramx);
  double fr_paramy; nh_p->getParam("/impedance_controller_schunk/FRy",fr_paramy);

  double mypoint; nh_p->getParam("/impedance_controller_schunk/mypoint",mypoint);
 
  XR<<xr_param,yr_param,zr_param;
  FR<< fr_param, fr_paramy, fr_paramx;

  M(0,0)=1*m_param;
  B(0,0)=1*b_param;  
  K(0,0)=1*k_param;

  M(1,1)=1*m_param;
  B(1,1)=1*b_param;  
  K(1,1)=1*k_param;

  M(2,2)=1*m_param;
  B(2,2)=1*b_param;  
  K(2,2)=1*k_param;

  kp=kp_param ; kd=kd_param; ki=ki_param;

	F_M<< (double)msg->wrench.force.z, -(double)msg->wrench.force.y, -(double)msg->wrench.force.x; //, 


  //F_My<<(double)msg->wrench.force.y (double)msg->wrench.force.z;
  //pv_filter(F_M);
  Eigen::VectorXd Offset(3);
  Offset<<offset, offsety, offsetx;

  //double measure = (double)F_M(0);

  /*if ( F_M(0) >= Offset(0)+0.3){
    if (ros::Time::now().toSec() - T_model_old.toSec() > 0.02 ){
      T_model_old=ros::Time::now();


      F_model = (FR+Offset + F_model_old*T_model_konst/0.02)/(T_model_konst/0.02 +1);
      F_model_old=F_model;  

      std::cout<<"Sila modela je "<<F_model(0)<<std::endl;    
    }
  }
  else{
    F_model = Eigen::MatrixXd::Zero(1,1);
    F_model<<offset;
    F_model_old=F_model;
   
  }*/

   geometry_msgs::WrenchStamped wrench_msg; // Create msg
                    //Fill msg
      wrench_msg.header.stamp = ros::Time::now();
      wrench_msg.wrench.force.x = 0;
      wrench_msg.wrench.force.y = 0;
      wrench_msg.wrench.force.z = (double)F_model(0);
      wrench_msg.wrench.torque.x = 0;
      wrench_msg.wrench.torque.y = 0;
      wrench_msg.wrench.torque.z = 0;
  reference_model_publisher.publish(wrench_msg);

   geometry_msgs::WrenchStamped wrench_msg_reference; // Create msg
                    //Fill msg
      wrench_msg_reference.header.stamp = ros::Time::now();
      wrench_msg_reference.wrench.force.x = 0;
      wrench_msg_reference.wrench.force.y = 0;
      wrench_msg_reference.wrench.force.z = (double)(FR(0)+ Offset(0));
      wrench_msg_reference.wrench.torque.x = 0;
      wrench_msg_reference.wrench.torque.y = 0;
      wrench_msg_reference.wrench.torque.z = 0;
  reference_publisher.publish(wrench_msg_reference);





	E = FR + Offset - F_M;
	//std::cout<<"Force from filter \n"<<F_pv_k<<std::endl;

  Eigen::VectorXd xx(1);
  xx<< XR(0);

  if((F_M(0)>=FR(0) +Offset(0)) && fc_param)
    ref_passed=true;

  if(ref_passed && fc_param && (F_M(0)<=FR(0) +Offset(0))){
    //nh_p->setParam("/impedance_controller_schunk/ki",15);
    ref_passed = false;
  }
  
	impedance_filter(-E, XR);

  Eigen::VectorXd W_OLD = Eigen::MatrixXd::Zero(6,1);
  W_OLD =  direct_k(Q_current);
  //Eigen::VectorXd W_OLD= Eigen::VectorXd::Map(&w_old[0],6);

  if (fc_param && !last_pub && !mypoint){
    std::cout<<"\n Aiming predifined trajectory with force control\n";
    //WD<<X_C_k,XR(1),XR(2),e1,e2,e3;

    Eigen::MatrixXd q_hc;
    q_hc=hc->get_next_point(50);
    if (q_hc(0,3) == 1) last_pub = true;

    //double ww[6];
    Eigen::VectorXd WW = Eigen::MatrixXd::Zero(6,1);
    Eigen::VectorXd QHC = Eigen::MatrixXd::Zero(6,1);
    QHC = q_hc.col(0);
    WW = direct_k(QHC);
    //Eigen::VectorXd WW = Eigen::VectorXd::Map(&ww[0],6); //po putanji gdje bih trebao ici
    Eigen::VectorXd XCK(1);
    XCK<<X_C_k;
    WW(0)= WW(0) + (XCK(0) - WW(0));
    WD=WW;

    traj_vector = {};
    std_msgs::Header h;
    h.stamp = ros::Time::now();
    traj_vector.goal.trajectory.joint_names.push_back("arm_1_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_2_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_3_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_4_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_5_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_6_joint");
    //traj_vector_prep.goal_id.stamp = h.stamp;
    std::ostringstream convert;
    convert << h.stamp.nsec;
    traj_vector.goal_id.id = "impedance_controller" + convert.str();
    std_msgs::Header h2;
    ros::Time Tprep = ros::Time::now();
    h2.stamp.sec =0; 
    h2.stamp.nsec = (int)0.0*pow(10,9);
    traj_vector.goal.trajectory.header.stamp = h2.stamp; //kada da krene
    traj_vector.header.stamp = h2.stamp;
    //WHERE TO?
    Eigen::VectorXd Q_CMD=  ik_closest(WD, Q_current);

    Q_CMD = Q_current + Jacobian(Q_current, WD - direct_k(Q_current) );

    double deltaq;
    deltaq = (Q_CMD-Q_current).array().abs().maxCoeff();
    deltaq = sqrt(pow(deltaq,2));
    //trajectory_msgs::JointTrajectoryPoint point_current;
    trajectory_msgs::JointTrajectoryPoint point;
    for (int j = 0; j<DOF; j++)
    {
       
      if (j==2) {      
        point.positions.push_back((double)(-Q_CMD(j)));
        point.velocities.push_back(double(-q_hc(j,1)));
        point.accelerations.push_back(double(-q_hc(j,2)));
        
      }else{
        point.positions.push_back((double)(Q_CMD(j)));
        point.velocities.push_back(double(q_hc(j,1)));
        point.accelerations.push_back(double(q_hc(j,2)));
        
      }    

    }
    point.time_from_start.nsec = (int)(5e08);  
    point.time_from_start.sec = (int)(0);
    traj_vector.goal.trajectory.points.push_back(point);


  }else if (!fc_param && !last_pub && !mypoint){
    std::cout<<"\n Aiming predifined trajectory\n";
   /* WD<<XR(0),XR(1),XR(2),e1,e2,e3;
    X_C_k<<XR(0);
    X_C_k_minus2=X_C_k;
    X_C_k_minus1=X_C_k;*/
    Eigen::MatrixXd q_hc;
    q_hc=hc->get_next_point(50);
    if (q_hc(0,3) == 1) last_pub = true;

    Eigen::VectorXd WW = Eigen::MatrixXd::Zero(6,1);
    Eigen::VectorXd QHC = Eigen::MatrixXd::Zero(6,1);
    QHC = q_hc.col(0);
    WW = direct_k(QHC);

   

    //Eigen::VectorXd WW = Eigen::VectorXd::Map(&ww[0],6); //po putanji gdje bih trebao ici
    //Eigen::VectorXd XCK(1);
    //XCK<<X_C_k;
    //WW(0)= WW(0) + (XCK - WW(0));

    WD=WW;
    X_C_k<<WD(0);
    X_C_k_minus2=X_C_k;
    X_C_k_minus1=X_C_k;
    
    traj_vector = {};
    std_msgs::Header h;
    h.stamp = ros::Time::now();
    traj_vector.goal.trajectory.joint_names.push_back("arm_1_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_2_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_3_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_4_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_5_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_6_joint");
    //traj_vector_prep.goal_id.stamp = h.stamp;
    std::ostringstream convert;
    convert << h.stamp.nsec;
    traj_vector.goal_id.id = "impedance_controller" + convert.str();
    std_msgs::Header h2;
    ros::Time Tprep = ros::Time::now();
    h2.stamp.sec =0; 
    h2.stamp.nsec = (int)0.0*pow(10,9);
    traj_vector.goal.trajectory.header.stamp = h2.stamp; //kada da krene
    traj_vector.header.stamp = h2.stamp;
    //WHERE TO?
    Eigen::VectorXd Q_CMD=  ik_closest(WD, Q_current);

    Q_CMD = Q_current + Jacobian(Q_current, WD - direct_k(Q_current) );
    double deltaq;
    deltaq = (Q_CMD-Q_current).array().abs().maxCoeff();
    deltaq = sqrt(pow(deltaq,2));
    //trajectory_msgs::JointTrajectoryPoint point_current;
    std::cout<<"Stari Q\n"<<Q_current<<" \nNovi Q \n"<<Q_CMD<<std::endl;
    trajectory_msgs::JointTrajectoryPoint point;
    for (int j = 0; j<DOF; j++)
    {
       
      if (j==2) {      
        point.positions.push_back((double)(-Q_CMD(j)));
        point.velocities.push_back(double(-q_hc(j,1)));
        point.accelerations.push_back(double(-q_hc(j,2)));
        
      }else{
        point.positions.push_back((double)(Q_CMD(j)));
        point.velocities.push_back(double(q_hc(j,1)));
        point.accelerations.push_back(double(q_hc(j,2)));
        
      }    

    }
    point.time_from_start.nsec = (int)(8e08);  
    point.time_from_start.sec = (int)(0*round(deltaq/0.2));
    traj_vector.goal.trajectory.points.push_back(point);
  } else if (mypoint && !fc_param){
   // std::cout<<"\n Aiming mypoint\n";
    WD<<XR(0),XR(1),XR(2),e1,e2,e3;
    X_C_k<<WD(0), WD(1), WD(2);
    X_C_k_minus2=X_C_k;
    X_C_k_minus1=X_C_k;

    traj_vector = {};
    std_msgs::Header h;
    h.stamp = ros::Time::now();
    traj_vector.goal.trajectory.joint_names.push_back("arm_1_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_2_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_3_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_4_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_5_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_6_joint");
    //traj_vector_prep.goal_id.stamp = h.stamp;
    std::ostringstream convert;
    convert << h.stamp.nsec;
    traj_vector.goal_id.id = "impedance_controller" + convert.str();
    std_msgs::Header h2;
    ros::Time Tprep = ros::Time::now();
    h2.stamp.sec =0; 
    h2.stamp.nsec = (int)0.0*pow(10,9);
    traj_vector.goal.trajectory.header.stamp = h2.stamp; //kada da krene
    traj_vector.header.stamp = h2.stamp;
    //WHERE TO?
    Eigen::VectorXd Q_CMD=  ik_closest(WD, Q_current);
    double deltaq;
    deltaq = (Q_CMD-Q_current).array().abs().maxCoeff();
    deltaq = sqrt(pow(deltaq,2));
    //trajectory_msgs::JointTrajectoryPoint point_current;
    trajectory_msgs::JointTrajectoryPoint point;
    for (int j = 0; j<DOF; j++)
    {
       
      if (j==2) {      
        point.positions.push_back((double)(-Q_CMD(j)));
        point.velocities.push_back(double(0));//-q_hc(j,1)));
       // point.accelerations.push_back(double(-q_hc(j,2)));
        
      }else{
        point.positions.push_back((double)(Q_CMD(j)));
        point.velocities.push_back(double(0));//q_hc(j,1)));
       // point.accelerations.push_back(double(q_hc(j,2)))
        
      }    

    }
    point.time_from_start.nsec = (int)(5*pow(10, 8));  
    point.time_from_start.sec = (int)(round(deltaq/0.4));
    traj_vector.goal.trajectory.points.push_back(point);

  } else if (mypoint && fc_param){
    //std::cout<<"\n Aiming mypoint with force control\n";
    WD<<X_C_k,e1,e2,e3;  //XR(1),XR(2) 

    traj_vector = {};
    std_msgs::Header h;
    h.stamp = ros::Time::now();
    traj_vector.goal.trajectory.joint_names.push_back("arm_1_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_2_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_3_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_4_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_5_joint");
    traj_vector.goal.trajectory.joint_names.push_back("arm_6_joint");
    //traj_vector_prep.goal_id.stamp = h.stamp;
    std::ostringstream convert;
    convert << h.stamp.nsec;
    traj_vector.goal_id.id = "impedance_controller" + convert.str();
    std_msgs::Header h2;
    ros::Time Tprep = ros::Time::now();
    h2.stamp.sec =0; 
    h2.stamp.nsec = (int)0.0*pow(10,9);
    traj_vector.goal.trajectory.header.stamp = h2.stamp; //kada da krene
    traj_vector.header.stamp = h2.stamp;
    //WHERE TO?
    Eigen::VectorXd Q_CMD=  ik_closest(WD, Q_current);

    Q_CMD = Q_current + Jacobian(Q_current, WD - direct_k(Q_current) );
    double deltaq;
    deltaq = (Q_CMD-Q_current).array().abs().maxCoeff();
    deltaq = sqrt(pow(deltaq,2));
    //trajectory_msgs::JointTrajectoryPoint point_current;
    trajectory_msgs::JointTrajectoryPoint point;
    for (int j = 0; j<DOF; j++)
    {
       
      if (j==2) {      
        point.positions.push_back((double)(-Q_CMD(j)));
       //point.velocities.push_back(double(-q_hc(j,1)));
       // point.accelerations.push_back(double(-q_hc(j,2)));
        
      }else{
        point.positions.push_back((double)(Q_CMD(j)));
       // point.velocities.push_back(double(q_hc(j,1)));
        //point.accelerations.push_back(double(q_hc(j,2)))
        
      }    

    }
    int t_secs = (int)floor(deltaq/0.1); 
    int t_nsecs = (int)((deltaq/0.1 - (double)t_secs)*1e09 );//+5*1e08);
    std::cout<<"Sekunde su"<<t_secs<<" "<<t_nsecs<<"\n";
    point.time_from_start.nsec = (int)t_nsecs;  
    point.time_from_start.sec = (int)t_secs;

    std::cout<<"Sekunde za publish su su "<<point.time_from_start.sec<<"\n"<<point.time_from_start.nsec<<"\n";


    traj_vector.goal.trajectory.points.push_back(point);

  }
  
/*
  if (use_jacobian)
  {
    
    Eigen::VectorXd DW = WD-W_OLD;

    Eigen::VectorXd DQ= Jacobian(Q_current, DW);

    Eigen::VectorXd Q_CMD= Q_current; 

    trajectory_msgs::JointTrajectoryPoint point;
    deltaq = (Q_CMD-Q_current).maxCoeff();
    deltaq = sqrt(pow(deltaq,2));
    for (int j = 0; j<DOF; j++)
    {
       if (j==2){ 
        point.positions.push_back((double)(-Q_CMD(j)-DQ(j)));
      }else{
        point.positions.push_back((double)(Q_CMD(j)+DQ(j)));
     }
      
    }
    point.time_from_start.nsec = (int)(0.5*pow(10, 9));
    point.time_from_start.sec = (int)0;
    traj_vector.goal.trajectory.points.push_back(point);

  } else 
  { 
    Eigen::VectorXd Q_CMD=  ik_closest(WD, Q_current);

    deltaq = (Q_CMD-Q_current).array().abs().maxCoeff();
    deltaq = sqrt(pow(deltaq,2));
    //trajectory_msgs::JointTrajectoryPoint point_current;
    trajectory_msgs::JointTrajectoryPoint point;
    for (int j = 0; j<DOF; j++)
    {
       
      if (j==2) {
        point.positions.push_back((double)(-Q_CMD(j)));
        point.velocities.push_back(0.0);
      
      }else{
        point.positions.push_back((double)(Q_CMD(j)));
        point.velocities.push_back(0.0);
        
      }    
    }  



    //std::cout<<"DQ je ..."<<deltaq<<std::endl;
    point.time_from_start.nsec = (int)(5*pow(10, 9));  
    point.time_from_start.sec = (int)(round(deltaq/0.2));
   //std::cout<<"sekundi..."<<ceil(deltaq/0.15)<<std::endl;
    traj_vector.goal.trajectory.points.push_back(point);
  }*/

  //std::cout<<"Status: "<<status<<std::endl;

  Eigen::VectorXd diff = ((W_OLD.array()*1e05).round() != (WD.array()*1e05).round()).cast<double>();            

 /* for (unsigned int i=0; i<POSITION.size(); i++){
    trajectory_msgs::JointTrajectoryPoint point;
    for (unsigned int j=0;j<POSITION[i].size(); j++){

       if (j==2) {
        point.positions.push_back((double)(-POSITION[i][j]));
        point.velocities.push_back(0);
      
      }else{
        point.positions.push_back((double)(POSITION[i][j]));
        point.velocities.push_back(0);
        
      } 
    }

    point.time_from_start.nsec = (int)0.5*pow(10, 9);
    if (i==0)
      point.time_from_start.sec = (int)(1+1)*1;//(i+1)*(int)(ceil(0.1/0.3));
    else
       point.time_from_start.sec = (int)(i+1+2)*1;
    traj_vector.goal.trajectory.points.push_back(point);
    
    
}
  trajectory_msgs::JointTrajectoryPoint point2;
  for(unsigned i=0; i<DOF;i++){
    point2.positions.push_back((double)(0));
    point2.velocities.push_back(0);
  }
  point2.time_from_start.nsec = (int)0.5*pow(10, 9);
  point2.time_from_start.sec = (int)(POSITION.size()+1+5)*1;
  traj_vector.goal.trajectory.points.push_back(point2);*/

  //std::cout<<"Postoji greska...s"<<diff.any()<<std::endl;

  if (move&&((check_status && status==3 && status_old==1 && diff.any()) || (check_status && status==3 && status_old==5 && diff.any()) || ( status==3 && status_old != 5 && diff.any()))){
    status_rdy=true;
    check_status=false;
  }
 

//  printf("\n STATUS JE %u  \n", status);

  //if (0 || (znopra&&move) || (1&&move && diff.any() && status==3) || (1&&move && status_old==1 && status==3 && diff.any()))
  //  makse=true;//((status ==3 && (ros::Time::now().toSec() - T_traj.toSec() >last_traj.stamp.sec))*0 || znopra)&&move)//&& diff.any()) || SecondPass  || (status ==3  && move && diff.any() ))//((T_traj.toSec() - ros::Time::now().toSec() )> ceil( deltaq/0.3))  && move && 1
  double always; nh_p->getParam("/impedance_controller_schunk/always",always);
  if (status_rdy || always)
  {
    //makse=false;
    //if(znopra) znopra=false;
    status_rdy=false;
    check_status=true;

    T_traj=ros::Time::now();
    //std::cout<<"Publisham trajektoriju\n";
    pub_traj_.publish(traj_vector);
  }

}

void ImpedanceController::pv_filter(Eigen::VectorXd& F_M)
{

  //std::cout<<"Filtering signal...\n";
  T_d = (double) (ros::Time::now().toSec() - T_old_f.toSec());
  if(T_d > 0.02){

    nh_p->getParam("/impedance_controller_schunk/TF",T_f);
    T_old_f = ros::Time::now();
    F_pv_k_minus1=F_pv_k;
    F_pv_k = (F_M + (T_f/T_d)*F_pv_k_minus1)/(T_f/T_d + 1);
  }
}


Eigen::VectorXd ImpedanceController::Jacobian(const Eigen::VectorXd& q, const Eigen::VectorXd& dw)
{   

  double l1,l2,l3,l4;
     //specific for robot
  l1=0.205; l2=0.3692; l3=0.316; l4=0.3183;

  l1=1000*l1; l2=1000*l2; l3=1000*l3; l4=1000*l4;
    //initaial JOINT conditions   
  double q0[] = {0,PI/2,PI/2,0,0,0};

  //Eigen::VectorXd Q0=Eigen::VectorXd::Map(&q0[0],6);


  double  q1=  wrapToPI(q(0) + q0[0]);
  double  q2=  wrapToPI(q(1) + q0[1]);
  double  q3=  wrapToPI(q(2) + q0[2]);
  double  q4=  wrapToPI(q(3) + q0[3]);
  double  q5=  wrapToPI(q(4) + q0[4]);
  double  q6=  wrapToPI(q(5) + q0[5]);

  Eigen::MatrixXd J=Eigen::MatrixXd::Zero(6,6);

  J(0,0) =l4*cos(q2 + q3)*cos(q4)*sin(q1)*sin(q5) - l2*cos(q2)*sin(q1) - l4*cos(q1)*sin(q4)*sin(q5) - sin(q2 + q3)*sin(q1)*(l3 + l4*cos(q5));
  J(0,1) =cos(q2 + q3)*cos(q1)*(l3 + l4*cos(q5)) - l2*cos(q1)*sin(q2) + l4*sin(q2 + q3)*cos(q1)*cos(q4)*sin(q5);
  J(0,2) =cos(q2 + q3)*cos(q1)*(l3 + l4*cos(q5)) + l4*sin(q2 + q3)*cos(q1)*cos(q4)*sin(q5);
  J(0,3) =l4*cos(q2 + q3)*cos(q1)*sin(q4)*sin(q5) - l4*cos(q4)*sin(q1)*sin(q5);
  J(0,4) =- l4*sin(q2 + q3)*cos(q1)*sin(q5) - l4*cos(q5)*sin(q1)*sin(q4) - l4*cos(q2 + q3)*cos(q1)*cos(q4)*cos(q5);
  J(0,5) =0;

  J(1,0) =sin(q2 + q3)*cos(q1)*(l3 + l4*cos(q5)) + l2*cos(q1)*cos(q2) - l4*sin(q1)*sin(q4)*sin(q5) - l4*cos(q2 + q3)*cos(q1)*cos(q4)*sin(q5);
  J(1,1) =cos(q2 + q3)*sin(q1)*(l3 + l4*cos(q5)) - l2*sin(q1)*sin(q2) + l4*sin(q2 + q3)*cos(q4)*sin(q1)*sin(q5);
  J(1,2) =cos(q2 + q3)*sin(q1)*(l3 + l4*cos(q5)) + l4*sin(q2 + q3)*cos(q4)*sin(q1)*sin(q5);
  J(1,3) =l4*cos(q1)*cos(q4)*sin(q5) + l4*cos(q2 + q3)*sin(q1)*sin(q4)*sin(q5);
  J(1,4) =l4*cos(q1)*cos(q5)*sin(q4) - l4*sin(q2 + q3)*sin(q1)*sin(q5) - l4*cos(q2 + q3)*cos(q4)*cos(q5)*sin(q1);
  J(1,5) =0;

  J(2,0) =0;
  J(2,1) =sin(q2 + q3)*(l3 + l4*cos(q5)) + l2*cos(q2) - l4*cos(q2 + q3)*cos(q4)*sin(q5);
  J(2,2) =sin(q2 + q3)*(l3 + l4*cos(q5)) - l4*cos(q2 + q3)*cos(q4)*sin(q5);
  J(2,3) =l4*sin(q2 + q3)*sin(q4)*sin(q5);
  J(2,4) =l4*cos(q2 + q3)*sin(q5) - l4*sin(q2 + q3)*cos(q4)*cos(q5);
  J(2,5) =0;

  J(3,0) =-exp(q6/PI)*(sin(q5)*(cos(q1)*sin(q4) - cos(q2 + q3)*cos(q4)*sin(q1)) + sin(q2 + q3)*cos(q5)*sin(q1));
  J(3,1) =exp(q6/PI)*(cos(q2 + q3)*cos(q1)*cos(q5) + sin(q2 + q3)*cos(q1)*cos(q4)*sin(q5));
  J(3,2) =exp(q6/PI)*(cos(q2 + q3)*cos(q1)*cos(q5) + sin(q2 + q3)*cos(q1)*cos(q4)*sin(q5));
  J(3,3) =-exp(q6/PI)*sin(q5)*(cos(q4)*sin(q1) - cos(q2 + q3)*cos(q1)*sin(q4));
  J(3,4) =-exp(q6/PI)*(cos(q5)*(sin(q1)*sin(q4) + cos(q2 + q3)*cos(q1)*cos(q4)) + sin(q2 + q3)*cos(q1)*sin(q5));
  J(3,5) =-(exp(q6/PI)*(sin(q5)*(sin(q1)*sin(q4) + cos(q2 + q3)*cos(q1)*cos(q4)) - sin(q2 + q3)*cos(q1)*cos(q5)))/PI;

  J(4,0) =-exp(q6/PI)*(sin(q5)*(sin(q1)*sin(q4) + cos(q2 + q3)*cos(q1)*cos(q4)) - sin(q2 + q3)*cos(q1)*cos(q5));
  J(4,1) =exp(q6/PI)*(cos(q2 + q3)*cos(q5)*sin(q1) + sin(q2 + q3)*cos(q4)*sin(q1)*sin(q5));
  J(4,2) =exp(q6/PI)*(cos(q2 + q3)*cos(q5)*sin(q1) + sin(q2 + q3)*cos(q4)*sin(q1)*sin(q5));
  J(4,3) =exp(q6/PI)*sin(q5)*(cos(q1)*cos(q4) + cos(q2 + q3)*sin(q1)*sin(q4));
  J(4,4) =exp(q6/PI)*(cos(q5)*(cos(q1)*sin(q4) - cos(q2 + q3)*cos(q4)*sin(q1)) - sin(q2 + q3)*sin(q1)*sin(q5));
  J(4,5) =(exp(q6/PI)*(sin(q5)*(cos(q1)*sin(q4) - cos(q2 + q3)*cos(q4)*sin(q1)) + sin(q2 + q3)*cos(q5)*sin(q1)))/PI;

  J(5,0) =0;
  J(5,1) =exp(q6/PI)*(sin(q2 + q3)*cos(q5) - cos(q2 + q3)*cos(q4)*sin(q5));
  J(5,2) =exp(q6/PI)*(sin(q2 + q3)*cos(q5) - cos(q2 + q3)*cos(q4)*sin(q5));
  J(5,3) =exp(q6/PI)*sin(q2 + q3)*sin(q4)*sin(q5);
  J(5,4) =exp(q6/PI)*(cos(q2 + q3)*sin(q5) - sin(q2 + q3)*cos(q4)*cos(q5));
  J(5,5) =-(exp(q6/PI)*(cos(q2 + q3)*cos(q5) + sin(q2 + q3)*cos(q4)*sin(q5)))/PI;



  Eigen::MatrixXd J_inv=pseudoInverse(J);

  Eigen::VectorXd dq=J_inv*dw;

  Eigen::VectorXd q_min(6);
  Eigen::VectorXd q_max(6);

  q_min<< -170,-110,-155,-170,-140,-170;  
  q_min=PI*q_min/180;
  q_max=-q_min;

 /* if ((((q+dq).array().unaryExpr(&wrapToPI) > q_max.array()).any()) + (((q+dq).array().unaryExpr(&wrapToPI) < q_min.array()).any()) ){
    std::cout<<"Vracam 0 0 0 0 0 0 \n";
    return Eigen::VectorXd::Zero(6);


  }*/

  return dq;

  


}

void ImpedanceController::impedance_filter(const Eigen::VectorXd& E, const Eigen::VectorXd& X_R)
{
	E_k=E;
  
  if (SecondPass)
  {	
  	SecondPass=false;
  	FirstPass=false;
  	T_old=ros::Time::now();
  	X_C_k_minus2=X_C_k_minus1;
  	X_C_k_minus1=X_C_k;

  	E_k_minus1=E;
  	

  } else if (FirstPass) 
  {
  	FirstPass=false;
  	SecondPass=true;  	
  	T_old=ros::Time::now();
  	X_C_k_minus1 = X_C_k;
  	E_k_minus1=E;
  	

  } else 
  {

  	T_d = (double) (ros::Time::now().toSec() - T_old.toSec());
  	//std::cout<<"discretization time is \n"<<T_d<<std::endl;
  	if (T_d>0.02)
  	{
	  	T_old=ros::Time::now();
	  	a= (1/pow(T_d,2))*M + (1/T_d)*B + K;
	  	b= (2/pow(T_d,2))*M + (1/T_d)*B;
	  	c= (-1/pow(T_d,2))*M;
	  	d= K;
	  	//std::cout<<"a matrix is\n"<<a<<std::endl;
	  	//std::cout<<"b matrix is\n"<<b<<std::endl;
	  	//std::cout<<"c matrix is\n"<<c<<std::endl;
	  	//std::cout<<"d matrix is\n"<<d<<std::endl;

	  	Eigen::VectorXd xr(1);     

      //if(ki){

        Up=kp*E_k;

        Ud=(kd/T_d)*(E_k - E_k_minus1);

        Ui= Ui_k_minus1 + ki*E_k*T_d;
        

        xr=X_R + Up + Ud + Ui;


        E_k_minus1=E_k;


        

    /*  }else{

        Up=kp*E_k;

        Ud=(kd/T_d)*(E_k - E_k_minus1);
        xr=X_R + Up + Ud + Ui*0;
        E_k_minus1=E_k;
        Ui.fill(0);
        Ui_k_minus1.fill(0);


      }*/


      

	  	X_C_k = a.inverse() * (E + b*X_C_k_minus1 + c*X_C_k_minus2 + d*xr);

      Eigen::MatrixXd feasible(6,1);
      feasible<<X_C_k, WD(3), WD(4), WD(5);  //WD(1), WD(2)
      Eigen::MatrixXd pomo = ik_closest(feasible, Q_current);
     // std::cout<<"Pokusavam dohvatiti "<<pomo<<std::endl;
      if (unfeasible){
        std::cout<<"UNFEASIBLE"<<std::endl;
        X_C_k= X_C_k_minus1;
        Ui = Ui_k_minus1;
      }

      Ui_k_minus1=Ui;


	  	std::cout<<"\n \n \n X commanded is\n \n \n"<<X_C_k<<std::endl;

	  	X_C_k_minus2=X_C_k_minus1;
	  	X_C_k_minus1=X_C_k;

  	}



  }

}



Eigen::MatrixXd ImpedanceController::direct_k(Eigen::MatrixXd q_sent)
{
    //double* qp;
    //qp=q_sent; 

    double l1,l2,l3,l4;
    double q[6], w[6];

    //copying INPUT
    for(int i=0; i<6; i++ ){ q[i] = q_sent(i,0); } 

     //specific for robot
    l1=0.205; l2=0.3692; l3=0.316; l4=0.3183;
    l1=1000*l1; l2=1000*l2; l3=1000*l3; l4=1000*l4;

     
    //initaial JOINT conditions   
    double q0[] = {0,PI/2,PI/2,0,0,0};

    q[0]=  wrapToPI(q[0] + q0[0]);
    q[1]=  wrapToPI(q[1] + q0[1]);
    q[2]=  wrapToPI(q[2] + q0[2]);
    q[3]=  wrapToPI(q[3] + q0[3]);
    q[4]=  wrapToPI(q[4] + q0[4]);
    q[5]=  wrapToPI(q[5] + q0[5]);

    w[0] = l2*cos(q[0])*cos(q[1])+cos(q[0])*(l3+l4*cos(q[4]))*sin(q[1]+q[2])-l4*cos(q[0])*cos(q[1]+q[2])*cos(q[3])*sin(q[4])-l4*sin(q[0])*sin(q[3])*sin(q[4]);
    w[1] = l2*cos(q[1])*sin(q[0])+(l3+l4*cos(q[4]))*sin(q[0])*sin(q[1]+q[2])-l4*cos(q[1]+q[2])*cos(q[3])*sin(q[0])*sin(q[4])+l4*cos(q[0])*sin(q[3])*sin(q[4]);
    w[2] = l1-cos(q[1]+q[2])*(l3+l4*cos(q[4]))+l2*sin(q[1])-l4*cos(q[3])*sin(q[1]+q[2])*sin(q[4]);
    w[3] = exp(q[5]/PI)*(cos(q[0])*cos(q[4])*sin(q[1]+q[2])-(cos(q[0])*cos(q[1]+q[2])*cos(q[3])+sin(q[0])*sin(q[3]))*sin(q[4]));
    w[4] = exp(q[5]/PI)*(cos(q[4])*sin(q[0])*sin(q[1]+q[2])+(-cos(q[1]+q[2])*cos(q[3])*sin(q[0])+cos(q[0])*sin(q[3]))*sin(q[4]));
    w[5] = exp(q[5]/PI)*(-cos(q[1]+q[2])*cos(q[4])-cos(q[3])*sin(q[1]+q[2])*sin(q[4]));   

    Eigen::MatrixXd RET_Value = Eigen::VectorXd::Map(&w[0],6);

    return RET_Value;
}


Eigen::VectorXd ImpedanceController::ik_closest(const Eigen::VectorXd& w1, const Eigen::VectorXd& q0){

  Eigen::MatrixXd q1;

  unfeasible=false;
  q1=inverse_k(w1);

  	
  for (int k=q1.cols(); k>0;k--)
  {
    if (q1(4, k-1)< 2e-016)
      q1(3, k-1)=q0(3);
  }


  int k_min=q1.cols()-1;

  if (k_min>0)
  {
    Eigen::VectorXd p=q1.col(q1.cols()-1)-q0;
    double d_min=(p.unaryExpr(&wrapToPI)).lpNorm<Eigen::Infinity>();
    for (int k=k_min;k>=0;k--)
    {
      p=q1.col(k)-q0;
      double d=(p.unaryExpr(&wrapToPI)).lpNorm<Eigen::Infinity>();
      if (d<d_min)
      {
        k_min=k;
        d_min=d;   
      }
    }

    p=q1.col(k_min);
    return p;

  } else if (k_min==0)
  {

    Eigen::VectorXd p=q1.col(k_min);
    return p;
  } else
  {
    unfeasible=true;
    return q0; //Eigen::VectorXd::Zero(6);
  }

}




Eigen::MatrixXd ImpedanceController::inverse_k(const Eigen::VectorXd& w)
{
  std::complex<double> l1(205,0);  
  std::complex<double> l2(369.2,0);
  std::complex<double> l3(316,0);
  std::complex<double> l4(318.3,0);
 // l1=1000*l1; l2=1000*l2; l3=1000*l3; l4=1000*l4;

  std::complex<double> w1, w2, w3, w4, w5, w6;
  std::complex<double>  r1, r2, r3;

  w1.real() = w(0); w1.imag() = 0;
  w2.real() = w(1); w2.imag() = 0;
  w3.real() = w(2); w3.imag() = 0;
  w4.real() = w(3); w4.imag() = 0; 
  w5.real() = w(4); w5.imag() = 0;
  w6.real() = w(5); w6.imag() = 0;
 
  Eigen::MatrixXcd q = Eigen::MatrixXcd::Zero(8,6);

  // Wrist roll
  std::complex<double> const1 =  PIC*log(sqrt(pow(w4,2) + pow(w5,2) + pow(w6,2)));
  q.col(5) = Eigen::VectorXcd::Constant(8,const1); 
  
  r1 = w4/exp(q(0,5)/PIC);
  r2 = w5/exp(q(0,5)/PIC);
  r3 = w6/exp(q(0,5)/PIC);

  // Waist
  q.block(0,0,4,1) = Eigen::MatrixXcd::Constant(4,1,(std::complex<double>)atan2((w2-l4*r2).real(), (w1-l4*r1).real()));
  q.block(4,0,4,1) = Eigen::MatrixXcd::Constant(4,1,(std::complex<double>)wrapToPI(q(0,0).real() +PI));

  // Auxiliary variables
  std::complex<double> p1 = w3 -l1 - l4*r3;
  Eigen::VectorXcd p2 = w1*cos(q.col(0).array()) + w2*sin(q.col(0).array()) - l4*(r1*cos(q.col(0).array())+r2*sin(q.col(0).array()));


  Eigen::VectorXcd ONE = Eigen::VectorXcd::Constant(8, std::complex<double>(1,0));

  // Elbow
  q.col(2)=((pow(p1,2)*ONE.array() + p2.array().square() - ONE.array()*pow(l2,2) - pow(l3,2)*ONE.array())/((std::complex<double>)2*l2*l3)).unaryExpr(&Asin);

  q.block(2,2,2,1) = (q.block(2,2,2,1)).unaryExpr(&sign)*PIC - q.block(2,2,2,1);
  q.block(6,2,2,1) = q.block(6,2,2,1).unaryExpr(&sign)*PIC - q.block(6,2,2,1); 

  // Shoulder
  // We are discarding the imaginary part to supress warnings. Infeasible
  // solutions will show up with imaginary q5.
  q.col(1) = ((( p1 * ( l2*ONE.array() + l3*sin(q.col(2).array()) ) + l3*p2.array()*(cos(q.col(2).array())) ).real()).
    binaryExpr( (p2.array()*(l2*ONE.array() + l3*sin(q.col(2).array())) - p1*l3*cos(q.col(2).array())).real(), &Atan2)).cast<std::complex<double> >();       
            
  // Wrist PItch
  q(0,4) = Acos((r1*cos(q(0,0)) + r2*sin(q(0,0)))*(sin(q(0,1)+q(0,2))) - r3*cos(q(0,1)+ q(0,2)));

  q(2,4) = Acos((r1*cos(q(2,0)) + r2*sin(q(2,0)))*(sin(q(2,1)+q(2,2))) - r3*cos(q(2,1)+ q(2,2)));

  q(4,4) = Acos((r1*cos(q(4,0)) + r2*sin(q(4,0)))*(sin(q(4,1)+q(4,2))) - r3*cos(q(4,1)+ q(4,2)));

  q(6,4) = Acos((r1*cos(q(6,0)) + r2*sin(q(6,0)))*(sin(q(6,1)+q(6,2))) - r3*cos(q(6,1)+ q(6,2)));

  q(1,4)=-q(0,4);   q(3,4)=-q(2,4);   q(5,4)=-q(4,4);   q(7,4)=-q(6,4);

  // Elbow roll
  // !!! Undefined when q5 = 0
  /// We are discarding the imaginary part to supress warnings. Infeasible
  // solutions will show up with imaginary q5.  
  q.col(3) =( ( sin(q.col(4).array()) * (-r1*sin(q.col(0).array())+ r2*cos(q.col(0).array())) ).real().binaryExpr( (
                                              sin(q.col(4).array())*( -(r1*cos(q.col(0).array()) + r2*sin(q.col(0).array()))*cos((q.col(1)+q.col(2)).array()) 
                                               -r3*sin((q.col(1)+q.col(2)).array()) )).real()  , &Atan2)).cast<std::complex<double> >();

  // When q5=0 set q4 to "default" value, which is PI in my kinematic model. PI is on a REAL ROBOT
  for (int i=0; i<8; i++)
  {
    if (q(i,4).real() < 2*eps)
      q(i,3)=(std::complex<double>)0;//PIC;

  }
             
  // Filter infeasible points.
  // Infeasible points contain imaginary joint rotations.
  // TODO: Are there any other indicators of infeasibility?
  // TODO: Currently, the imaginary cutoff value is arbitrary,
  //       is there a better way to PIck a cutoff
 // std::cout<<q<<std::endl;
  Eigen::MatrixXd outliers = (abs(q.imag().array()) > 0.001).cast<double>();
  Eigen::VectorXd discard = Eigen::VectorXd::Constant(8,0);

  Eigen::MatrixXd q0(1,6);
  q0<<0,PI/2,PI/2,0,0,0;

  Eigen::MatrixXd q_min(1,6);
  Eigen::MatrixXd q_max(1,6);

  q_min<< -170,-110,-155,-170,-140,-170;
  q_min=PI*q_min/180;
  q_max=-q_min;

  for (int i=0; i<8; i++)
  {

    Eigen::MatrixXd q_POM = (q.row(i).real().array() -q0.array()).unaryExpr(&wrapToPI);
    // Filter out points outside of joint limits and unfeasible solutions
    if (outliers.row(i).any() + (q_POM.array() > q_max.array()).any() + (q_POM.array() < q_min.array()).any()  ) 
     discard(i)=1; 
    else 
      q.row(i) = q_POM.cast< std::complex<double> >();


  }

  //create new matrix for feasible solutions
  Eigen::MatrixXd filtered1(8-discard.count(),6);

  int j=0;
  for (int i=0; i<8; i++)
  {

    if (discard(i)==0)
    {
      filtered1.row(j)=q.row(i).real();
      j++;
    }
  }
  // if there is no feasible solutions return Q_CMD=0 X6
  if(j==0)
    return (q0).transpose();

  Eigen::MatrixXd filtered=filtered1.transpose();

  // Filter false solutions
  // These solutions are generated by fusing redundant joint solutions
  // that actually don' t match yield the original pose
  // TODO: Be more careful when fusing redundant solutions to avoid this
  // problem altogether
  Eigen::MatrixXd ret_val;
  std::vector<double> entries;
  int rows=0;
  int cols=0;
  //Eigen::MatrixXd w_k;
  
  for (int i=0; i<filtered.cols(); i++)
  { 
    double q_tmp[6];
    Eigen::MatrixXd q_tmpE=Eigen::MatrixXd::Zero(6,1);
    q_tmpE.col(0) = filtered.col(i);    
    Eigen::VectorXd w_K = Eigen::MatrixXd::Zero(6,1);
    w_K = direct_k(q_tmpE);
    //Eigen::VectorXd w_K= Eigen::VectorXd::Map(&w_k[0],6);
    Eigen::Map<Eigen::MatrixXd>(q_tmp, q_tmpE.rows(), q_tmpE.cols()) = q_tmpE; 

    if ((w-w_K).lpNorm<Eigen::Infinity>() <= 1e-6)
    { 
      std::vector<double> pom;
      pom.assign(q_tmp, q_tmp + 6);
      entries.insert(entries.end(), pom.begin(), pom.end()); 
      cols++; rows=6;
      
    }

  }  

  ret_val = Eigen::MatrixXd::Map(&entries[0], 6, cols);

  return ret_val;

}




    

Eigen::MatrixXd ImpedanceController::taylor_path(Eigen::MatrixXd q1_tay, Eigen::MatrixXd w2_tay, double tol_tay, int nrec_tay)
{
  std::cout<<nrec_tay<<std::endl;
   
  Eigen::MatrixXd q2_tay, qm_tay, wm_tay, wM_tay, qM_tay, q_solve1, q_solve1_wM, q_solve2, q_return;

  //double q1tay[6];

  //Eigen::Map<Eigen::MatrixXd>(q1tay, q1_tay.rows(), q1_tay.cols()) = q1_tay;


  //Eigen::MatrixXd w1_tay = Eigen::MatrixXd::Zero(6,1);

  Eigen::MatrixXd w1_tay = direct_k(q1_tay);

  

  //w1_tay = Eigen::VectorXd::Map(&w1tay[0], 6);
    
  q2_tay = ik_closest(w2_tay, q1_tay);


  if(q1_tay.cols() > 0 && q2_tay.cols() > 0)
  {
    qm_tay = (q1_tay + q2_tay)/2;
    //double qmtay[6];

    //Eigen::Map<Eigen::MatrixXd>(qmtay, qm_tay.rows(), qm_tay.cols()) = qm_tay;
   
   // Eigen::MatrixXd wmtay = Eigen::MatrixXd::Zero(6,1);

    wm_tay = direct_k(qm_tay);

    //wm_tay = Eigen::VectorXd::Map(&wmtay[0], 6);
    wM_tay = Eigen::MatrixXd::Zero(6,1);
    
    wM_tay = (w1_tay + w2_tay)/2;
    //std::cout<<"\nDO TU\n";
    qM_tay = ik_closest(wM_tay, q1_tay);
    

    if( ( ((wm_tay.block(0,0,3,1) - wM_tay.block(0,0,3,1)).lpNorm<Eigen::Infinity>()) > tol_tay) && nrec_tay < 1000)
    {

      q_solve1_wM = taylor_path(q1_tay, wM_tay, tol_tay, nrec_tay+1);
      

      q_solve2 = taylor_path(qM_tay, w2_tay, tol_tay, nrec_tay+1);
      
      q_solve1 = Eigen::MatrixXd::Zero(q_solve1_wM.rows(), q_solve1_wM.cols()-1);
      q_solve1 = q_solve1_wM.block(0,0,q_solve1_wM.rows(),q_solve1_wM.cols()-1);
      
      q_return = Eigen::MatrixXd::Zero(q_solve1.rows(), q_solve1.cols() + q_solve2.cols());
      q_return << q_solve1, q_solve2;
      
    }
    else
    {
      q_return = Eigen::MatrixXd::Zero(q1_tay.rows(), q1_tay.cols() + q2_tay.cols());
      q_return << q1_tay, q2_tay;
    }

    return q_return;
  }
}

Eigen::MatrixXd ImpedanceController::taylor_path_multPoint(Eigen::MatrixXd path_tay, double tol_tay)
{
  //path_tay_ret, path_tay_help, path_tay_save;

  //double path_tay_array[6];
 // Eigen::Map<Eigen::MatrixXd>(path_tay_array, path_tay.col(1).rows(), path_tay.col(1).cols()) = path_tay.col(1);

  //double w_tay_array[6];
  Eigen::MatrixXd path_tay_col = Eigen::MatrixXd::Zero(6,1);
  path_tay_col = direct_k(path_tay.col(1));
 
   //path_tay_col= Eigen::VectorXd::Map(&w_tay_array[0], 6);

  //std::cout<<"dslkjfdksjfkdsj\n"<<path_tay.col(0)<<"dkhfkjčdsh"<<path_tay_col<<"dsngkdjćlkfč";


  path_tay_ret = taylor_path(path_tay.col(0), path_tay_col, tol_tay, 0);

   
  for (int i = 1; i < path_tay.cols()-1; i++)
  {
    Eigen::MatrixXd path_tay_help, path_tay_save;

    //Eigen::Map<Eigen::MatrixXd>(path_tay_array, path_tay.col(i+1).rows(), path_tay.col(i+1).cols()) = path_tay.col(i+1);

    path_tay_col = direct_k(path_tay.col(i+1));
    //path_tay_col = Eigen::VectorXd::Map(&w_tay_array[0], 6);


    path_tay_help = taylor_path(path_tay.col(i), path_tay_col, tol_tay, 0);

    //save current path
    path_tay_save = Eigen::MatrixXd::Zero(path_tay_ret.rows(), path_tay_ret.cols()-1); //-1
    path_tay_save = path_tay_ret.block(0,0,path_tay_ret.rows(),path_tay_ret.cols()-1); //-1

    path_tay_ret.resize(path_tay_save.rows(), path_tay_save.cols()+path_tay_help.cols());
    path_tay_ret = Eigen::MatrixXd::Zero(path_tay_save.rows(), path_tay_save.cols()+path_tay_help.cols());
    path_tay_ret << path_tay_save, path_tay_help;

   
    } 
      Eigen::MatrixXd path_tay_help, path_tay_save;
      //Eigen::Map<Eigen::MatrixXd>(path_tay_array, path_tay.col(0).rows(), path_tay.col(0).cols()) = path_tay.col(0);
      path_tay_col = direct_k(path_tay.col(0));
      //path_tay_col = Eigen::VectorXd::Map(&w_tay_array[0], 6);


      path_tay_help = taylor_path(path_tay.col(path_tay.cols()-1), path_tay_col, tol_tay, 0);

      //save current path
      path_tay_save = Eigen::MatrixXd::Zero(path_tay_ret.rows(), path_tay_ret.cols()-1); //-1
      path_tay_save = path_tay_ret.block(0,0,path_tay_ret.rows(),path_tay_ret.cols()-1); //-1

      path_tay_ret.resize(path_tay_save.rows(), path_tay_save.cols()+path_tay_help.cols());
      path_tay_ret = Eigen::MatrixXd::Zero(path_tay_save.rows(), path_tay_save.cols()+path_tay_help.cols());
      path_tay_ret << path_tay_save, path_tay_help;

  return path_tay_ret;
} 

Eigen::MatrixXd ImpedanceController::ik_path(Eigen::MatrixXd w_ik_path)
{
  Eigen::MatrixXd q_root, q_ret, q_path_help, q_path_save;
  q_root = Eigen::MatrixXd::Zero(6,1);
  q_ret = ik_closest(w_ik_path.col(0), q_root);
  for (int i = 1; i < w_ik_path.cols(); i++)
  {
    Eigen::MatrixXd v1 = w_ik_path.col(i);
    Eigen::MatrixXd v2 = q_ret.col(i-1);
    q_path_help = Eigen::MatrixXd::Zero(6,1);
    q_path_help = ik_closest(v1, v2);
    q_path_save = Eigen::MatrixXd::Zero(q_ret.rows(), q_ret.cols());
    q_path_save = q_ret;
    q_ret = Eigen::MatrixXd::Zero(q_path_save.rows(), q_path_save.cols() + q_path_help.cols());
    q_ret << q_path_save, q_path_help;
  }


  return q_ret;

}        

int main( int argc, char** argv )
{ 
    

  ros::init(argc, argv, "impedance_controller_schunk");
  ros::NodeHandle nh, nh_ns("~");
  //std::string configFile;      
 


  nh_p=&nh;  
  ros::Rate r(50);
  ImpedanceController Force_Controller(nh, nh_ns);
  

  

  //std::cout<<F.Return_UFC(-166.5,-20)<<std::endl;

  //std::cout<<(-1 * std::numeric_limits<float>::infinity()/0);

  while(ros::ok())
  { 

    ros::spinOnce();
    r.sleep();
  }
 // ros::spin();
    
  ROS_INFO( "Quitting... " );


  return 0;
}
