#include "kinematics.h"

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Eigen>




double wrapToPI( double angle )
{
   int num2pi = floor(angle/(2*PI) + 0.5);
   return angle - num2pi*2*PI;
}

std::complex<double> sign(std::complex<double> a){
  if (a.real() == 0 && a.imag() == 0 ) 
    return (std::complex<double>) 0;
  else 
    return a/(abs(a));
}

double Atan2(double a, double b){
  return atan2(a,b);
}

std::complex<double> Asin (std::complex<double> a)
{
  std::complex<double> i(0,1);
  return -i*log(i*a +pow((std::complex<double>)1-pow(a,2),0.5));
}

std::complex<double> Acos (std::complex<double> a)
{
  std::complex<double> i(0,1);
  return -i*log(a +i*pow((std::complex<double>)1-pow(a,2),0.5));
}




ImpedanceController::ImpedanceController (ros::NodeHandle& n, ros::NodeHandle& n_ns)
{

  //SUBSCRIBER 
  joint_sub = n.subscribe("/lwa4p/joint_states", 100, &ImpedanceController::JointStateCallback, this);
  force_sub = n.subscribe("/lwa4p/ft_sensor_topic", 10, &ImpedanceController::ForceCallback, this);


  //PUBLISHERS
 // position_pub = n.advertise<std_msgs::Float64MultiArray>("tool_pose", 100);
  pose_pub = n.advertise<geometry_msgs::PoseStamped>("TP", 100);

  joint_1_cmd = n.advertise<std_msgs::Float64>("/lwa4p/arm_1_joint_pos_controller/command",100);
  joint_2_cmd = n.advertise<std_msgs::Float64>("/lwa4p/arm_2_joint_pos_controller/command",100);
  joint_3_cmd = n.advertise<std_msgs::Float64>("/lwa4p/arm_3_joint_pos_controller/command",100);
  joint_4_cmd = n.advertise<std_msgs::Float64>("/lwa4p/arm_4_joint_pos_controller/command",100);
  joint_5_cmd = n.advertise<std_msgs::Float64>("/lwa4p/arm_5_joint_pos_controller/command",100);
  joint_6_cmd = n.advertise<std_msgs::Float64>("/lwa4p/arm_6_joint_pos_controller/command",100);

  /*
  ToolPose.layout.dim.push_back(std_msgs::MultiArrayDimension());
  ToolPose.layout.dim.push_back(std_msgs::MultiArrayDimension());
  ToolPose.layout.dim[0].label = "height";
  ToolPose.layout.dim[0].size = 1;
  ToolPose.layout.dim[0].stride = 1*1;
  ToolPose.layout.dim[1].label = "length";
  ToolPose.layout.dim[1].size = 6;
  ToolPose.layout.dim[1].stride = 1;
  ToolPose.layout.data_offset = 0;*/
  
  q1_cmd.data=0;
  q2_cmd.data=0;
  q3_cmd.data=0;
  q4_cmd.data=0;
  q5_cmd.data=0;
  q6_cmd.data=0;

  eps=1e-5;

  //initalizing impedance filter

  X_C_k_minus1 = Eigen::VectorXd::Zero(3);
  X_C_k_minus2 = Eigen::VectorXd::Zero(3);
  X_C_k =Eigen::VectorXd::Zero(3);
  

  E_k = Eigen::VectorXd::Zero(3);
  E_k_minus1 = E_k; 
  XR=Eigen::VectorXd::Zero(3);
  XR<<-500,0,1000;

  X_C_k=XR;
  X_C_k_minus2=X_C_k;
  X_C_k_minus1=X_C_k;
  
  
  M = Eigen::MatrixXd::Identity(3,3);//.cast<double>();
  B = Eigen::MatrixXd::Identity(3,3);//.cast<double>();
  K = Eigen::MatrixXd::Identity(3,3);//.cast<double>();
  M(0,0)=M(0,0)*5e3/(pow(65.7,2));
  M(1,1)=1;
  M(2,2)=1;

  B(0,0)=B(0,0)*2*0.7*65.7*M(0,0);
 // B(1,1)=B(1,1)*2*0.7*65.7*M(0,0);
 //B(2,2)=B(2,2)*2*0.7*65.7*M(0,0);
  B(1,1)=1;
  B(2,2)=1;
  K(0,0)=K(0,0)*5e3;
 // K(1,1)=K(1,1)*5e8;
  //K(2,2)=K(2,2)*5e8;
  K(1,1)=1;
  K(2,2)=1;

  FirstPass = true; 
  SecondPass = false;
  T_d= 0.01; //discretization
  T_old=ros::Time::now();

  kp=-0.005; kd=-0.000;

  Q_current=Eigen::VectorXd::Zero(6);

  
}





void ImpedanceController::JointStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
  double q[6];
  double w[6];
  //double w_desired[]={   -505,0,1000,-1,0, 0.0000};	

  Start=ros::Time::now();

  //copying position
  for(int i=0; i<6; i++) 
  { 
    q[i] = (double)(msg->position[i]); 
  }

  //Eigen::VectorXd WD= Eigen::VectorXd::Map(&w_desired[0], 6);
  Q_current = Eigen::VectorXd::Map(&q[0], 6);
  


  direct_k(q,w);
  std::cout<<"X = "<<w[0]<<" Y = "<<w[1]<<" Z = "<<w[2]<<" Euler 1 = "<<w[3]<<" Euler 2 = "<<w[4]<<" Euler 3 = "<<w[5]<<std::endl;

  //TP is tool pose
  TP.pose.position.x=(double)w[0];
  TP.pose.position.y=(double)w[1];
  TP.pose.position.z=(double)w[2];

  TP.pose.orientation.x=(double)w[3];
  TP.pose.orientation.y=(double)w[4];
  TP.pose.orientation.z=(double)w[5];
  TP.pose.orientation.w=1000.01;

  TP.header.stamp = ros::Time::now();
  TP.header.frame_id = "/world";

  pose_pub.publish(TP);

  End=ros::Time::now();

  //std::cout<<"Time elapsed seconds: \n"<<(End-Start).toSec()<<std::endl;
 // std::cout<<"Time elapsed nanoseconds: \n"<<(End-Start)<<std::endl;
}
//kod cijeli prebaciti da rade po klasma, u nodeove cak posebno odvojiti kinematiku od vanjske kontrole, doci do prijenosnih zglobova
void ImpedanceController::ForceCallback(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{

	Eigen::VectorXd F_pv(3);
	Eigen::VectorXd FR(3);
	Eigen::VectorXd E(3);
	
	Eigen::VectorXd WD(6);

	

	std::cout<<"Measured force X = \n"<<msg->wrench.force.x<<std::endl;

	std::cout<<"Measured force Y = \n"<<msg->wrench.force.y<<std::endl;

	std::cout<<"Measured force Z = \n"<<msg->wrench.force.z<<std::endl;

	F_pv<< (double)msg->wrench.force.x, (double)msg->wrench.force.y, (double)msg->wrench.force.z;

	FR<<0,0,0.02;

	E = FR - F_pv;
	std::cout<<"Force error is \n"<<E<<std::endl;

	//if(msg->wrench.force.x > 0.3){
		double KP;
		std::cout << "Succes is "<<n.getParam("/impedance_controller/kp", KP)<<"Fetched parameter is"<<KP<<std::endl;
		impedance_filter(E, XR);
		WD<<X_C_k,-1,0,0;
	//}
	//else
	//	WD<<XR,-1,0,0;

	

	//std::cout<<"Desired position is \n"<<WD<<"Current joints position "<<Q_current<<std::endl;

  	Eigen::VectorXd Q_CMD=  ik_closest(WD, Q_current);

	//std::cout<<"OCE OCE\n";

  	
  
  	//std::cout<<"Q_CMD=\n"<<Q_CMD<<std::endl;  

	q1_cmd.data=(double)Q_CMD(0);
	q2_cmd.data=(double)Q_CMD(1);
	q3_cmd.data=(double)Q_CMD(2);
	q4_cmd.data=(double)Q_CMD(3);
	q5_cmd.data=(double)Q_CMD(4);
	q6_cmd.data=(double)Q_CMD(5);

	/*q1_cmd.data=(double)0;
	q2_cmd.data=(double)0;
	q3_cmd.data=(double)0;
	q4_cmd.data=(double)0;
	q5_cmd.data=(double)0;
	q6_cmd.data=(double)0;*/
	
	joint_1_cmd.publish(q1_cmd);
	joint_2_cmd.publish(q2_cmd);  
	joint_3_cmd.publish(q3_cmd);
	joint_4_cmd.publish(q4_cmd);
	joint_5_cmd.publish(q5_cmd);
	joint_6_cmd.publish(q6_cmd);


}


void ImpedanceController::impedance_filter(const Eigen::VectorXd& E, const Eigen::VectorXd& X_R)
{
	E_k=E;
  if (SecondPass)
  {	
  	SecondPass=false;
  	FirstPass=false;
  	T_old=ros::Time::now();
  	X_C_k_minus2=X_C_k_minus1;
  	X_C_k_minus1=X_C_k;

  	E_k_minus1=E;
  	

  } else if (FirstPass) 
  {
  	FirstPass=false;
  	SecondPass=true;  	
  	T_old=ros::Time::now();
  	X_C_k_minus1 = X_C_k;
  	E_k_minus1=E;
  	

  } else 
  {

  	T_d = (double) (ros::Time::now().toSec() - T_old.toSec());
  	//std::cout<<"discretization time is \n"<<T_d<<std::endl;
  	if (T_d>0.01)
  	{
	  	T_old=ros::Time::now();
	  	a= (1/pow(T_d,2))*M + (1/T_d)*B + K;
	  	b= (2/pow(T_d,2))*M + (1/T_d)*B;
	  	c= (-1/pow(T_d,2))*M;
	  	d= K;
	  	//std::cout<<"a matrix is\n"<<a<<std::endl;
	  	//std::cout<<"b matrix is\n"<<b<<std::endl;
	  	//std::cout<<"c matrix is\n"<<c<<std::endl;
	  	//std::cout<<"d matrix is\n"<<d<<std::endl;

	  	Eigen::VectorXd xr(3);
	  	xr=XR + kp*E + (kd/T_d)*(E_k - E_k_minus1);
	  	E_k_minus1=E_k;

	  	X_C_k = a.inverse() * (E + b*X_C_k_minus1 + c*X_C_k_minus2 + d*xr);
	  	std::cout<<"X commanded is\n"<<X_C_k<<std::endl;

	  	X_C_k_minus2=X_C_k_minus1;
	  	X_C_k_minus1=X_C_k;

  	}



  }

}



void ImpedanceController::direct_k(double* q_sent,  double*  w)
{
    double* qp;
    qp=q_sent; 

    double l1,l2,l3,l4;
    double q[6];

    //copying INPUT
    for(int i=0; i<6; i++ ){ q[i] = q_sent[i]; } 

     //specific for robot
    l1=600; l2=350; l3=305; l4=160;
     
    //initaial JOINT conditions   
    double q0[] = {0,PI/2,PI/2,PI,0,0};

    q[0]=  wrapToPI(q[0] + q0[0]);
    q[1]=  wrapToPI(q[1] + q0[1]);
    q[2]=  wrapToPI(q[2] + q0[2]);
    q[3]=  wrapToPI(q[3] + q0[3]);
    q[4]=  wrapToPI(q[4] + q0[4]);
    q[5]=  wrapToPI(q[5] + q0[5]);

    w[0] = l2*cos(q[0])*cos(q[1])+cos(q[0])*(l3+l4*cos(q[4]))*sin(q[1]+q[2])-l4*cos(q[0])*cos(q[1]+q[2])*cos(q[3])*sin(q[4])-l4*sin(q[0])*sin(q[3])*sin(q[4]);
    w[1] = l2*cos(q[1])*sin(q[0])+(l3+l4*cos(q[4]))*sin(q[0])*sin(q[1]+q[2])-l4*cos(q[1]+q[2])*cos(q[3])*sin(q[0])*sin(q[4])+l4*cos(q[0])*sin(q[3])*sin(q[4]);
    w[2] = l1-cos(q[1]+q[2])*(l3+l4*cos(q[4]))+l2*sin(q[1])-l4*cos(q[3])*sin(q[1]+q[2])*sin(q[4]);
    w[3] = exp(q[5]/PI)*(cos(q[0])*cos(q[4])*sin(q[1]+q[2])-(cos(q[0])*cos(q[1]+q[2])*cos(q[3])+sin(q[0])*sin(q[3]))*sin(q[4]));
    w[4] = exp(q[5]/PI)*(cos(q[4])*sin(q[0])*sin(q[1]+q[2])+(-cos(q[1]+q[2])*cos(q[3])*sin(q[0])+cos(q[0])*sin(q[3]))*sin(q[4]));
    w[5] = exp(q[5]/PI)*(-cos(q[1]+q[2])*cos(q[4])-cos(q[3])*sin(q[1]+q[2])*sin(q[4]));    
}


Eigen::VectorXd ImpedanceController::ik_closest(const Eigen::VectorXd& w1, const Eigen::VectorXd& q0){

  Eigen::MatrixXd q1;


  q1=inverse_k(w1);

  	
  for (int k=q1.cols(); k>0;k--)
  {
    if (q1(4, k-1)< 2*0.001)
      q1(3, k-1)=q0(3);
  }


  int k_min=q1.cols()-1;

  if (k_min>0)
  {
    Eigen::VectorXd p=q1.col(q1.cols()-1)-q0;
    double d_min=(p.unaryExpr(&wrapToPI)).lpNorm<Eigen::Infinity>();
    for (int k=k_min;k>=0;k--)
    {
      p=q1.col(k)-q0;
      double d=(p.unaryExpr(&wrapToPI)).lpNorm<Eigen::Infinity>();
      if (d<d_min)
      {
        k_min=k;
        d_min=d;   
      }
    }

    p=q1.col(k_min);
    return p;

  } else if (k_min==0)
  {

    Eigen::VectorXd p=q1.col(k_min);
    return p;
  } else
  {
    return q0;
  }

}




Eigen::MatrixXd ImpedanceController::inverse_k(const Eigen::VectorXd& w)
{
  std::complex<double> l1(600,0);
  std::complex<double> l2(350,0);
  std::complex<double> l3(305,0);
  std::complex<double> l4(160,0);

  std::complex<double> w1, w2, w3, w4, w5, w6;
  std::complex<double>  r1, r2, r3;

  w1.real() = w(0); w1.imag() = 0;
  w2.real() = w(1); w2.imag() = 0;
  w3.real() = w(2); w3.imag() = 0;
  w4.real() = w(3); w4.imag() = 0; 
  w5.real() = w(4); w5.imag() = 0;
  w6.real() = w(5); w6.imag() = 0;
 
  Eigen::MatrixXcd q = Eigen::MatrixXcd::Zero(8,6);

  // Wrist roll
  std::complex<double> const1 =  PIC*log(sqrt(pow(w4,2) + pow(w5,2) + pow(w6,2)));
  q.col(5) = Eigen::VectorXcd::Constant(8,const1); 
  
  r1 = w4/exp(q(0,5)/PIC);
  r2 = w5/exp(q(0,5)/PIC);
  r3 = w6/exp(q(0,5)/PIC);

  // Waist
  q.block(0,0,4,1) = Eigen::MatrixXcd::Constant(4,1,(std::complex<double>)atan2((w2-l4*r2).real(), (w1-l4*r1).real()));
  q.block(4,0,4,1) = Eigen::MatrixXcd::Constant(4,1,(std::complex<double>)wrapToPI(q(0,0).real() +PI));

  // Auxiliary variables
  std::complex<double> p1 = w3 -l1 - l4*r3;
  Eigen::VectorXcd p2 = w1*cos(q.col(0).array()) + w2*sin(q.col(0).array()) - l4*(r1*cos(q.col(0).array())+r2*sin(q.col(0).array()));


  Eigen::VectorXcd ONE = Eigen::VectorXcd::Constant(8, std::complex<double>(1,0));

  // Elbow
  q.col(2)=((pow(p1,2)*ONE.array() + p2.array().square() - ONE.array()*pow(l2,2) - pow(l3,2)*ONE.array())/((std::complex<double>)2*l2*l3)).unaryExpr(&Asin);

  q.block(2,2,2,1) = q.block(2,2,2,1).unaryExpr(&sign)*PIC - q.block(2,2,2,1);
  q.block(6,2,2,1) = q.block(6,2,2,1).unaryExpr(&sign)*PIC - q.block(6,2,2,1); 

  // Shoulder
  // We are discarding the imaginary part to supress warnings. Infeasible
  // solutions will show up with imaginary q5.
  q.col(1) = ((( p1 * ( l2*ONE.array() + l3*sin(q.col(2).array()) ) + l3*p2.array()*(cos(q.col(2).array())) ).real()).
    binaryExpr( (p2.array()*(l2*ONE.array() + l3*sin(q.col(2).array())) - p1*l3*cos(q.col(2).array())).real(), &Atan2)).cast<std::complex<double> >();       
            
  // Wrist PItch
  q(0,4) = Acos((r1*cos(q(0,0)) + r2*sin(q(0,0)))*(sin(q(0,1)+q(0,2))) - r3*cos(q(0,1)+ q(0,2)));

  q(2,4) = Acos((r1*cos(q(2,0)) + r2*sin(q(2,0)))*(sin(q(2,1)+q(2,2))) - r3*cos(q(2,1)+ q(2,2)));

  q(4,4) = Acos((r1*cos(q(4,0)) + r2*sin(q(4,0)))*(sin(q(4,1)+q(4,2))) - r3*cos(q(4,1)+ q(4,2)));

  q(6,4) = Acos((r1*cos(q(6,0)) + r2*sin(q(6,0)))*(sin(q(6,1)+q(6,2))) - r3*cos(q(6,1)+ q(6,2)));

  q(1,4)=-q(0,4);   q(3,4)=-q(2,4);   q(5,4)=-q(4,4);   q(7,4)=-q(6,4);

  // Elbow roll
  // !!! Undefined when q5 = 0
  /// We are discarding the imaginary part to supress warnings. Infeasible
  // solutions will show up with imaginary q5.  
  q.col(3) =( ( sin(q.col(4).array()) * (-r1*sin(q.col(0).array())+ r2*cos(q.col(0).array())) ).real().binaryExpr( (
                                              sin(q.col(4).array())*( -(r1*cos(q.col(0).array()) + r2*sin(q.col(0).array()))*cos((q.col(1)+q.col(2)).array()) 
                                               -r3*sin((q.col(1)+q.col(2)).array()) )).real()  , &Atan2)).cast<std::complex<double> >();

  // When q5=0 set q4 to "default" value, which is PI in my kinematic model. PI is on a REAL ROBOT
  for (int i=0; i<8; i++)
  {
    if (q(i,4).real() < 2*eps)
      q(i,3)=(std::complex<double>)0;
  }
             
  // Filter infeasible points.
  // Infeasible points contain imaginary joint rotations.
  // TODO: Are there any other indicators of infeasibility?
  // TODO: Currently, the imaginary cutoff value is arbitrary,
  //       is there a better way to PIck a cutoff
 // std::cout<<q<<std::endl;
  Eigen::MatrixXd outliers = (abs(q.imag().array()) > 0.001).cast<double>();
  Eigen::VectorXd discard = Eigen::VectorXd::Constant(8,0);

  Eigen::MatrixXd q0(1,6);
  q0<<0,PI/2,PI/2,PI,0,0;

  Eigen::MatrixXd q_min(1,6);
  Eigen::MatrixXd q_max(1,6);

  q_min<< -170,-110,-155,-170,-140,-170;
  q_min=PI*q_min/180;
  q_max=-q_min;

  for (int i=0; i<8; i++)
  {

    Eigen::MatrixXd q_POM = (q.row(i).real().array() -q0.array()).unaryExpr(&wrapToPI);
    // Filter out points outside of joint limits and unfeasible solutions
    if (outliers.row(i).any() + (q_POM.array() > q_max.array()).any() + (q_POM.array() < q_min.array()).any()  ) 
     discard(i)=1; 
    else 
      q.row(i) = q_POM.cast< std::complex<double> >();


  }

  //create new matrix for feasible solutions
  Eigen::MatrixXd filtered1(8-discard.count(),6);

  int j=0;
  for (int i=0; i<8; i++)
  {

    if (discard(i)==0)
    {
      filtered1.row(j)=q.row(i).real();
      j++;
    }
  }
  // if there is no feasible solutions return Q_CMD=0 X6
  if(j==0)
    return (q0-q0).transpose();

  Eigen::MatrixXd filtered=filtered1.transpose();

  // Filter false solutions
  // These solutions are generated by fusing redundant joint solutions
  // that actually don' t match yield the original pose
  // TODO: Be more careful when fusing redundant solutions to avoid this
  // problem altogether
  Eigen::MatrixXd ret_val;
  std::vector<double> entries;
  int rows=0;
  int cols=0;
  double w_k[6];
  
  for (int i=0; i<filtered.cols(); i++)
  {
    double *q_tmp=(double*)filtered.col(i).data();    
    direct_k(q_tmp, w_k);
    Eigen::VectorXd w_K= Eigen::VectorXd::Map(&w_k[0],6); 

    if ((w-w_K).lpNorm<Eigen::Infinity>() <= 1e-6)
    { 
      std::vector<double> pom;
      pom.assign(q_tmp, q_tmp + 6);
      entries.insert(entries.end(), pom.begin(), pom.end()); 
      cols++; rows=6;
      
    }

  }  

  ret_val = Eigen::MatrixXd::Map(&entries[0], 6, cols);

  return ret_val;

}


               

int main( int argc, char** argv )
{ 
  ros::init(argc, argv, "impedance_controller");
  ros::NodeHandle nh, nh_ns("~");
  //std::string configFile;

  

  //std::string path = ros::package::getPath("schunk_lwa4p_control");

    //getting ros params
  //nh_ns.param("config_file", configFile, path + std::string("/config/lwa4p_configuration.yaml"));
  
  ros::Rate r(100);
  ImpedanceController Force_Controller(nh, nh_ns);
  while(ros::ok())
  {

    ros::spinOnce();
    r.sleep();
  }
 // ros::spin();
    
  ROS_INFO( "Quitting... " );

  return 0;
}
